package auiEditor.client.myObjects;

/**
 * Class used to get an object representation of abstract interaction unit
 * @author Vi Tran 
 * @author Jonathan Ricetti
 *
 */
public class AbstractIUnit{
	
	/**
	 * Abstract interaction unit name
	 * 
	 * @see AbstractIUnit#setName(String)
	 * @see AbstractIUnit#getName()
	 */
    private String name;
    
    /**
     * Abstract interaction unit type
     * 
     * @see AbstractIUnit#setType(String)
     * @see AbstractIUnit#getType()
     */
	private String type;
	
	/**
	 * Abstract interaction unit description
	 * 
	 * @see AbstractIUnit#setDescription(String)
	 * @see AbstractIUnit#getDescription()
	 */
	private String description = "";
	
	/**
	 * Abstract interaction unit parentID
	 * 
	 * @see AbstractIUnit#setParentID(int)
	 * @see AbstractIUnit#getParentId()
	 */
	private int parentId;
	
	/**
	 * Abstract interaction unit ID </br>
	 * Unique one ID per AIU cannot be changed
	 * 
	 * @see AbstractIUnit#getId()
	 */
	private int id;
	
	/**
	 * Abstract interaction unit position in x in canvas
	 * 
	 * @see AbstractIUnit#setX(int)
	 * @see AbstractIUnit#getX()
	 */
	private int x = 0;
	
	/**
	 * Abstract interaction unit position in y in canvas
	 * 
	 * @see AbstractIUnit#setY(int)
	 * @see AbstractIUnit#getY()
	 */
	private int y = 0;
	
	/**
	 * Abstract interaction unit width in canvas
	 * 
	 * @see AbstractIUnit#setWidth(int)
	 * @see AbstractIUnit#getWidth()
	 */
	private int w = 0; 
	
	/**
	 * Abstract interaction unit height in canvas
	 * 
	 * @see AbstractIUnit#setHeight(int)
	 * @see AbstractIUnit#getHeight()
	 */
	private int h = 0;
	
	/**
	 * boolean true if the AIU is selected false if not
	 * 
	 * @see AbstractIUnit#setSelected(boolean)
	 * @see AbstractIUnit#isSelected()
	 * 
	 */
	private boolean isSelected = false;
	
	/**
	 * boolean true if the AIU is expanded false if  the AIU is collapsed
	 * 
	 * @see AbstractIUnit#setExpanded(boolean)
	 * @see AbstractIUnit#isExpanded()
	 */
	private boolean isExpanded = true;
	  
	/**
	 * Constructor of {@link AbstractIUnit} </br>
	 * Set the field that characterize an AIU
	 * @param id id of the AIU
	 * @param name name of the AIU
	 * @param description description of the AIU
	 * @param type type of the AIU
	 * @param parentId parent id of the AIU
	 */
	public AbstractIUnit(int id, String name,String description, String type, int parentId)
	{
		this.id = id;
		this.name = name;
		this.description = description;
		this.type = type;
		this.parentId = parentId;
	}
	
	/**
	 * Switch the value of the {@link AbstractIUnit#isExpanded}
	 */
	public void switchExpanded(){
		this.isExpanded = !isExpanded;
	}
	
	/**
	 * set the value of x position
	 * @param x
	 * 		 {@link AbstractIUnit#x}
	 */
	public  void setX(int x)
	{
		this.x = x;
	}
	
	/**
	 * set the value of y position
	 * @param y
	 * 		{@link AbstractIUnit#y}
	 */
	public  void setY(int y)
	{
		this.y = y;
	}
	
	/**
	 * set the value of the weight 
	 * @param w
	 * 		 {@link AbstractIUnit#w}
	 */
	public  void setWidth(int w)
	{
		this.w = w;
	}
	
	/**
	 * set the value of the height
	 * @param h 
	 * : {@link AbstractIUnit#h}
	 */
	public  void setHeight(int h)
	{
		this.h = h;
	}
	
	/**
	 * return the id of the AIU
	 * @return {@link AbstractIUnit#id}
	 */
	public  int getId()
	{
		return this.id;
	}
	
	/**
	 * return the name of the AIU
	 * @return {@link AbstractIUnit#name}
	 */
	public  String getName()
	{
		return this.name;
	}
	
	/**
	 * return the description of the AIU
	 * @return {@link AbstractIUnit#description}
	 */
	public  String getDescription()
	{
		return this.description;
	}
	
	/**
	 * set  the name of the AIU
	 * @see AbstractIUnit#name
	 * @param name : name of the AIU
	 */
	public  void setName(String name)
	{
		this.name= name;
	}
	
	/**
	 * return the type of the AIU
	 * @return {@link AbstractIUnit#type}
	 */
	public  String getType()
	{
		return this.type;
	}
	
	/**
	 * set the type of the AIU
	 * @see AbstractIUnit#type
	 * @param type : AIU type
	 */
	public  void setType(String type)
	{
		this.type= type;
	}
	
	/**
	 * get the id of the parent AIU
	 * @return {@link AbstractIUnit#parentId}
	 */
	public  int getParentId()
	{
		return this.parentId;
	}
	
	/**
	 * set the  parent id
	 * @see AbstractIUnit#parentId
	 * @param id : parent id
	 */
	public  void setParentID(int id)
	{
		this.parentId = id;
	}
	
	/**
	 * get the x position
	 * @return  {@link AbstractIUnit#x}
	 */
	public  int getX()
	{
		return this.x;
	}
	
	/**
	 * get the y position
	 * @return {@link AbstractIUnit#y}
	 */
	public  int getY()
	{
		return this.y;
	}
	
	/**
	 * get the width of the AIU
	 * @return {@link AbstractIUnit#w}
	 */
	public  int getWidth()
	{
		return this.w;
	}
	/**
	 * get the height of the AIU
	 * @return {@link AbstractIUnit#h}
	 */
	public  int getHeight()
	{
		return this.h;
	}
	
	/**
	 * return if the AIU is selected
	 * @return {@link AbstractIUnit#isSelected}
	 */
	public  boolean isSelected()
	{
		return isSelected;
	}
	
	/**
	 * return if the AIU is expanded
	 * @return {@link AbstractIUnit#isExpanded}
	 */
	public  boolean isExpanded()
	{
		return isExpanded;
	}
	
	/**
	 * set the AIU selected 
	 * @see AbstractIUnit#isSelected
	 * @param isSelected is the AIU selected
	 */
	public  void setSelected(boolean isSelected)
	{
		this.isSelected = isSelected;
	}
	
	/**
	 * set the AIU expanded or collapsed
	 * @see AbstractIUnit#isExpanded
	 * @param isExpanded is the AIU expanded or collapsed
	 */
	public void setExpanded(boolean isExpanded)
	{
		this.isExpanded = isExpanded;
	}
	
	/**
	 * set the description of the AIU
	 * @see AbstractIUnit#description
	 * @param description : description of the AIU
	 */
	public void setDescription(String description) {
		this.description = description;
		
	}
}
