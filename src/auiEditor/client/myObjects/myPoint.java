package auiEditor.client.myObjects;

/**
 * Class representing a point in the edior canvas
 * @author Vi Tran
 *
 */
public class MyPoint {
	
	/**
	 * x position of the point
	 * @see MyPoint#getX()
	 */
	private int x = 0;
	
	/**
	 * y position of the point
	 * @see MyPoint#getY()
	 */
	private int y = 0;
	
	/**
	 * Contructor of the Point class 
	 * @param x position in x 
	 * @param y position in y
	 */
	public MyPoint(int x, int y) {
		this.x = x;
		this.y = y;
		
	}
	
	/**
	 * return the position in x of the point
	 * @return {@link MyPoint#x}
	 */
	public int getX()
	{
		return x;
	}
	
	/**
	 * return the position in y of the point
	 * 
	 * @return {@link MyPoint#y}
	 */
	public int getY()
	{
		return y;
	}

}
