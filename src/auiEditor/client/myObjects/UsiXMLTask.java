package auiEditor.client.myObjects;

import auiEditor.client.AEditor;

/**
 * Class used to get an object representation of UsiXML task
 * @author Vi Tran
 * @author Jonathan Ricetti
 */
public class UsiXMLTask{
	
	/**
	 * id of the task should be unique
	 * 
	 * @see UsiXMLTask#setTaskId(int)
	 * @see UsiXMLTask#getTaskID()
	 */
	private int taskId;
	
	/**
	 * id of the parent task
	 * 
	 * @see UsiXMLTask#setParentID(int)
	 * @see UsiXMLTask#getParentID()
	 */
	private int parentId;
	
	/**
	 * id of the task at the right of the this task in the task tree
	 * 
	 * @see UsiXMLTask#setRightTaskID(int)
	 * @see UsiXMLTask#getRightTaskID()
	 * @see UsiXMLTask#taskTemporalization
	 */
	private int rightTaskId;
	
	/**
	 *  name of the task
	 *  
	 *  @see UsiXMLTask#setTaskName(String)
	 *  @see UsiXMLTask#getTaskName()
	 */
	private String taskName = "";
	
	/**
	 * type of the task
	 * 
	 * @see UsiXMLTask#setTaskType(String)
	 * @see UsiXMLTask#getTaskType()
	 */
	private String taskType= "";
	
	/**
	 * temporal operator between this task and its right task 
	 * 
	 * @see UsiXMLTask#setTaskTemporalization(String)
	 * @see UsiXMLTask#getTaskTemporalization()
	 * @see UsiXMLTask#rightTaskId
	 */
	private String taskTemporalization = "";
	
	/**
	 * description of the task
	 * 
	 * @see UsiXMLTask#setDescription(String)
	 * @see UsiXMLTask#getDescription()
	 */
	private String description = "";	
	
	/**
	 *  x position of the task in the canvas
	 *  
	 *  @see UsiXMLTask#setX(int)
	 *  @see UsiXMLTask#getX()
	 */
	private int x = 0;
	
	/**
	 * y position of the task in the canvas
	 * 
	 * @see UsiXMLTask#setY(int)
	 * @see UsiXMLTask#getY()
	 */
	private int y = 0;
	
	/**
	 * group number of the task used to generate AUI model
	 * 
	 * @see UsiXMLTask#setGroupNumber(long)
	 * @see UsiXMLTask#getGroupNumber()
	 * @see AEditor#tasksToAUIs(int, int)
	 */
	long groupNumber = 0;
	
	/**
	 * weight of the task used for task platform adaptation
	 * 
	 * @see UsiXMLTask#setWeight(int)
	 * @see UsiXMLTask#getWeight()
	 * @see AEditor#tasksToAUIs(int, int)
	 */
	int weight = 0;
	
	/**
	 * mental workload of the task used for task mental workload adaptation
	 * 
	 * @see UsiXMLTask#setMentalWorload(int)
	 * @see UsiXMLTask#getMentalWorload()
	 * @see AEditor#tasksToAUIs(int, int)
	 */
	int mentalWorkload = 0;
	
	/**
	 * Constructor of UsiXMLTask </br>
	 * Create a task and set all its fields
	 * @param taskId id of the task
	 * @param parentId id of the parent task
	 * @param rightTaskId id of the right task
	 * @param taskName name of the task
	 * @param taskType type of the task
	 * @param taskTemporalization temporal operator of the task
	 * @param description description of the tasks
	 */
	public UsiXMLTask(int taskId, int parentId, int rightTaskId, String taskName, String taskType, String taskTemporalization, String description)
	{
		this.taskId = taskId;
		this.parentId = parentId;
		this.rightTaskId = rightTaskId;
		this.taskName = taskName;
		this.taskType = taskType;
		this.taskTemporalization = taskTemporalization;
		this.description = description;
		
	}
	
	/**
	 * Constructor of UsiXMLTask </br>
	 * Create an task without setting the fields
	 */
	public UsiXMLTask(){
		
	}
	
	/**
	 * set the id of the task
	 * @see UsiXMLTask#taskId
	 * @param taskID task id
	 */
	public  void setTaskId(int taskID)
	{
		this.taskId = taskID;
	}
	
	/**
	 * set the id of the parent task
	 * @see UsiXMLTask#parentId
	 * @param parentId id of the parent task
	 */
	public  void setParentID(int parentId)
	{
		this.parentId = parentId;
	}
	
	/**
	 * set the id of the right task
	 * @see UsiXMLTask#rightTaskId
	 * @param rightTaskId id of the right task
	 */
	public  void setRightTaskID(int rightTaskId)
	{
		this.rightTaskId = rightTaskId;
	}
	
	/**
	 * set the name of the task
	 * @see UsiXMLTask#taskName
	 * @param taskName the name of the task
	 */
	public  void setTaskName(String taskName)
	{
		this.taskName = taskName;
	}
	
	/**
	 * set the type of the task
	 * @see UsiXMLTask#taskType
	 * @param taskType type of the task
	 */
	public  void setTaskType(String taskType)
	{
		this.taskType = taskType;
	}
	
	/**
	 * set the temporal operator between the task and its right task
	 * @see UsiXMLTask#taskTemporalization
	 * @see UsiXMLTask#rightTaskId
	 * @param taskTemporalization temporal operator of the task
	 */
	public  void setTaskTemporalization(String taskTemporalization)
	{
		this.taskTemporalization = taskTemporalization;
	}
	
	/**
	 * description of the task
	 * @see UsiXMLTask#description
	 * @param description description of the task
	 */
	public  void setDescription(String description)
	{
		this.description = description;
	}
	
	/**
	 * set the weight of the task
	 * @see UsiXMLTask#weight
	 * @param w weight of the task
	 */
	public void setWeight(int w)
	{
		weight = w;
	}
	
	/**
	 * set the mental workload of the task
	 * @see UsiXMLTask#mentalWorkload
	 * @param mentalWorkload mental workload of the task
	 */
	public void setMentalWorload(int mentalWorkload)
	{
		this.mentalWorkload = mentalWorkload;
	}
	
	/**
	 * set the group number of the task
	 * @see UsiXMLTask#groupNumber
	 * @param groupNumber group number
	 */
	public  void setGroupNumber(long groupNumber)
	{
		this.groupNumber = groupNumber;
	}

	
	/**
	 * return the right task id
	 * @return {@link UsiXMLTask#rightTaskId}
	 */
	public  int getRightTaskID()
	{
		return this.rightTaskId;
	}
	
	/**
	 * return the task id
	 * 
	 * @return {@link UsiXMLTask#taskId}
	 */
	public  int getTaskID()
	{
		return this.taskId;
	}
	
	/**
	 * return the parent task id
	 * @return {@link UsiXMLTask#parentId}
	 */
	public  int getParentID()
	{
		return this.parentId;
	}
	
	/**
	 * return the task name
	 * @return {@link UsiXMLTask#taskName}
	 */
	public  String getTaskName()
	{
		return this.taskName;
	}
	
	/**
	 * return the task type
	 * @return {@link UsiXMLTask#taskType}
	 */
	public  String getTaskType()
	{
		return this.taskType;
	}
	
	/**
	 * return the task temporal operator
	 * @return {@link UsiXMLTask#taskTemporalization}
	 */
	public  String getTaskTemporalization()
	{
		return this.taskTemporalization;
	}
	
	/**
	 * return the task description 
	 * @return {@link UsiXMLTask#description}
	 */ 
	public  String getDescription()
	{
		return this.description;
	}
	
	/**
	 * set the position in x of the task
	 * @see UsiXMLTask#x
	 * @param x position in x
	 */
	public  void setX(int x)
	{
		this.x = x;
	}
	
	/**
	 * set the position in y of the task
	 * @see UsiXMLTask#y
	 * @param y position in y
	 */
	public  void setY(int y)
	{
		this.y = y;
	}
	
	/**
	 * return the position in x of the task
	 * @return {@link UsiXMLTask#x}
	 */
	public  int getX()
	{
		return this.x;
	}
	
	/**
	 * return the position in y of the task
	 * @return {@link UsiXMLTask#y}
	 */
	public  int getY()
	{
		return this.y;
	}
	
	/**
	 * return the group number of the task
	 * @return {@link UsiXMLTask#groupNumber}
	 */
	public long getGroupNumber()
	{
		return groupNumber;
	}
	
	/**
	 * return the weight of the task
	 * @return {@link UsiXMLTask#weight}
	 */
	public int getWeight()
	{
		return weight;
	}
	
	/**
	 * return the mental workload of the task
	 * @return {@link UsiXMLTask#mentalWorkload}
	 */
	public int getMentalWorload()
	{
		return mentalWorkload;
	}

}
