package auiEditor.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import auiEditor.client.myDialogs.AIUnitDialog;
import auiEditor.client.myDialogs.AIUEdition;
import auiEditor.client.myDialogs.MyPasteDialog;
import auiEditor.client.myDialogs.MyYesNoDialog;
import auiEditor.client.myDialogs.MySaveDialog;
import auiEditor.client.myDialogs.OpenAuiXml;
import auiEditor.client.myDialogs.OpenTaskModel;
import auiEditor.client.myDialogs.AUIGeneratorDialog;
import auiEditor.client.myDialogs.MyMessageDialog;
import auiEditor.client.myDialogs.MyViewOptionDialog;
import auiEditor.client.myObjects.AbstractIUnit;
import auiEditor.client.myObjects.UsiXMLTask;
import auiEditor.client.myObjects.MyPoint;
import auiEditor.server.FileDownloadServlet;

import com.google.gwt.canvas.client.Canvas;
import com.google.gwt.canvas.dom.client.Context2d;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.ImageElement;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Image;
import com.google.gwt.user.client.ui.KeyboardListener;
import com.google.gwt.user.client.ui.MouseListener;
import com.google.gwt.user.client.ui.PushButton;
import com.google.gwt.user.client.ui.ScrollPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

@SuppressWarnings({ "deprecation" })
/**
 * Main class of the editor
 * @author Vi Tran
 * @author Jonathan Ricetti
 */
public class AEditor extends DialogBox implements MouseListener, ClickListener,
		KeyboardListener {

	/**
	 * Not useful at this moment can be use later to set a access control
	 */
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);

	/**
	 * image of a selection AIU
	 */
	private Image imgSelect = null;

	/**
	 * image of an action AIU
	 */
	private Image imgAction = null;

	/**
	 * image of an input AIU
	 */
	private Image imgInput = null;

	/**
	 * image of a collapsed AIU container
	 */
	private Image imgUp = null;

	/**
	 * image of an expanded AIU container
	 */
	private Image imgDown = null;

	/**
	 * used when transforming the AIU objects into an XML representation of the
	 * AUI model
	 */
	private String sTextFile = "";

	/**
	 * Delete AIU button
	 */
	private PushButton bDel = new PushButton(new Image(GWT.getModuleBaseURL()
			+ "images/delete.png"));

	/**
	 * Copy AIU button
	 */
	private PushButton bCop = new PushButton(new Image(GWT.getModuleBaseURL()
			+ "images/copy.png"));

	/**
	 * Cut AIU button
	 */
	private PushButton bCut = new PushButton(new Image(GWT.getModuleBaseURL()
			+ "images/cut.png"));

	/**
	 * Paste AIU button
	 */
	private PushButton bPas = new PushButton(new Image(GWT.getModuleBaseURL()
			+ "images/paste.png"));

	/**
	 * Edit AIU button
	 */
	private PushButton bEdit = new PushButton(new Image(GWT.getModuleBaseURL()
			+ "images/edit.png"));

	/**
	 * Collapse AIU all button
	 */
	private PushButton bUp = new PushButton(new Image(GWT.getModuleBaseURL()
			+ "images/up.png"));

	/**
	 * Expand AIU all button
	 */
	private PushButton bDown = new PushButton(new Image(GWT.getModuleBaseURL()
			+ "images/down.png"));

	/**
	 * Switch Expand/Collapse AIU button
	 */
	private PushButton bExpand = new PushButton(new Image(
			GWT.getModuleBaseURL() + "images/collapse.png"));

	/**
	 * Editor option button
	 */
	private PushButton bOption = new PushButton(new Image(
			GWT.getModuleBaseURL() + "images/option.png"));

	/**
	 * Open task model button
	 */
	private PushButton bOpen = new PushButton(new Image(GWT.getModuleBaseURL()
			+ "images/open-icon.png"));

	/**
	 * Open AUI model button
	 */
	private PushButton bLoad = new PushButton(new Image(GWT.getModuleBaseURL()
			+ "images/load.png"));

	/**
	 * Save AUI model button
	 */
	private PushButton bSave = new PushButton(new Image(GWT.getModuleBaseURL()
			+ "images/save.png"));

	/**
	 * New AUI model button
	 */
	private PushButton bNew = new PushButton(new Image(GWT.getModuleBaseURL()
			+ "images/New.png"));

	/**
	 * Adaptation of task model to AUI model button
	 */
	private PushButton bSelection = new PushButton(new Image(
			GWT.getModuleBaseURL() + "images/select_down.png"));

	/**
	 * Add input AIU button
	 */
	private PushButton bInput = new PushButton(new Image(GWT.getModuleBaseURL()
			+ "images/input.png"));

	/**
	 * Add selection AIU button
	 */
	private PushButton bSelector = new PushButton(new Image(
			GWT.getModuleBaseURL() + "images/selection.png"));

	/**
	 * Add action AIU button
	 */
	private PushButton bAction = new PushButton(new Image(
			GWT.getModuleBaseURL() + "images/action.png"));

	/**
	 * Add output AIU button
	 */
	private PushButton bOutput = new PushButton(new Image(
			GWT.getModuleBaseURL() + "images/output.png"));

	/**
	 * Add AIU container button
	 */
	private PushButton bContainer = new PushButton(new Image(
			GWT.getModuleBaseURL() + "images/container.png"));

	/**
	 * true if the CTRL key is pressed, false if not
	 */
	private boolean isCtrlPressing = false;

	/**
	 * true if the selected AIU(s) was cut, false if not
	 */
	private boolean isCut = false;

	/**
	 * true if the selected AIU(s) was copied, false if not
	 */
	private boolean isCopy = false;

	/**
	 * index used for {@link AbstractIUnit} selection
	 */
	private int iIndex = 0;

	/**
	 * maximum number of {@link AbstractIUnit} per line
	 */
	private int columnNumber = 4;

	/**
	 * next available index for new {@link AbstractIUnit}
	 */
	private int componentId = -1;

	/**
	 * index used to generate UsiXML string of an AUI model
	 */
	private int componentIdXml = 0;
	/**
	 * Mouse position were you clicked
	 */
	private MyPoint p1 = new MyPoint(0, 0), p2 = new MyPoint(0, 0);

	/**
	 * Canvas where the AUI model is represented
	 */
	private Canvas canvas;

	/**
	 * Allow scrolling the canvas
	 */
	private ScrollPanel scroller;

	/**
	 * Drawing contact of the cavas
	 */
	private Context2d context2D;

	/**
	 * main panel of the editor
	 */
	private VerticalPanel vpMain = new VerticalPanel();

	/**
	 * main menu panel
	 */
	private HorizontalPanel hpMenu = new HorizontalPanel();

	/**
	 * edition menu
	 */
	private HorizontalPanel hpEdition = new HorizontalPanel();

	/**
	 * control menu
	 */
	private HorizontalPanel hpControl = new HorizontalPanel();

	/**
	 * file edition menu
	 */
	private HorizontalPanel hpFileEdition = new HorizontalPanel();

	/**
	 * canvas panel
	 */
	private VerticalPanel vp = new VerticalPanel();

	/**
	 * size of the canvas
	 */
	private int W = 2200, H = 3000;

	/**
	 * {@link AbstractIUnit} graphical representation size and space options
	 */
	int Xo = 10, Yo = 10, widthBetweenAIU = 20, heightBetweenAIU = 20,
			Width = 80, Height = 17;

	/**
	 * value of the adding AIU state </br>
	 * 
	 * -1: no button pressed, 0: Input, 1: Output,</br> 2: selection, 3: Action,
	 * 4 container
	 */
	private int iButtonIcon = -1;

	/**
	 * AUI model: list of the {@link AbstractIUnit}s in the model
	 */
	private Vector<AbstractIUnit> abstractIUnits = new Vector<AbstractIUnit>();

	/**
	 * list of the copied or cut {@link AbstractIUnit}s
	 */
	private Vector<AbstractIUnit> copiedAIUnits = new Vector<AbstractIUnit>();

	/**
	 * task model : list of the {@link UsiXMLTask}s in the model
	 */
	private Vector<UsiXMLTask> tasks = new Vector<UsiXMLTask>();

	/**
	 * Constructor of the {@link AEditor}
	 */
	public AEditor() {
		// not useful at this moment can be use to set access control
		verifyUser("", "");

		// set the title of the dialog box
		setText("W3C-compliant AUI Editor");

		// add ClickListener to the button
		bDel.addClickListener(this);
		bCop.addClickListener(this);
		bCut.addClickListener(this);
		bPas.addClickListener(this);
		bEdit.addClickListener(this);
		bExpand.addClickListener(this);
		bUp.addClickListener(this);
		bDown.addClickListener(this);

		bOpen.addClickListener(this);
		bLoad.addClickListener(this);
		bSave.addClickListener(this);
		bNew.addClickListener(this);
		bSelection.addClickListener(this);
		bOption.addClickListener(this);

		bInput.addClickListener(this);
		bOutput.addClickListener(this);
		bAction.addClickListener(this);
		bSelector.addClickListener(this);
		bContainer.addClickListener(this);

		// create the canva
		canvas = Canvas.createIfSupported();
		if (canvas == null) {
			Window.alert(" Canvas is not supported!");
			return;
		} else
			context2D = canvas.getContext2d();
		// add MouseListener and KeyboardListener to the canvas
		canvas.addMouseListener(this);
		canvas.addKeyboardListener(this);

		// the scroller with the canvas inside
		scroller = new ScrollPanel(canvas);

		// add button to the menu
		hpFileEdition.add(bOpen);
		hpFileEdition.add(bSelection);
		hpFileEdition.add(bLoad);
		hpFileEdition.add(bNew);
		hpFileEdition.add(bSave);
		hpMenu.add(hpFileEdition);

		hpEdition.add(bDel);
		hpEdition.add(bCop);
		hpEdition.add(bCut);
		hpEdition.add(bPas);
		hpEdition.add(bEdit);
		hpEdition.add(bExpand);
		hpEdition.add(bUp);
		hpEdition.add(bDown);
		hpEdition.add(bOption);
		hpMenu.add(hpEdition);

		hpControl.add(bInput);
		hpControl.add(bOutput);
		hpControl.add(bSelector);
		hpControl.add(bAction);
		hpControl.add(bContainer);
		hpMenu.add(hpControl);

		// add menu to the main panel
		vpMain.add(hpMenu);
		vpMain.add(vp);
		vpMain.add(scroller);
		setWidget(vpMain);

		// set the canvas size
		canvas.setCoordinateSpaceWidth(W);
		canvas.setCoordinateSpaceHeight(H);
		// set size and title of all buttons
		setControlSize();
		// init the image used for aiu representation
		initImages();
		// draw the AUI model
		drawAUIModel();

	}

	/**
	 * set buttons size and test
	 */
	private void setControlSize() {
		int w = 32, h = 32;
		bDel.setPixelSize(w, h);
		bCop.setPixelSize(w, h);
		bCut.setPixelSize(w, h);
		bPas.setPixelSize(w, h);
		bEdit.setPixelSize(w, h);
		bExpand.setPixelSize(w, h);
		bUp.setPixelSize(w, h);
		bDown.setPixelSize(w, h);
		bOption.setPixelSize(w, h);

		bOpen.setPixelSize(w, h);
		bLoad.setPixelSize(w, h);
		bNew.setPixelSize(w, h);
		bSave.setPixelSize(w, h);
		bSelection.setPixelSize(w, h);

		bInput.setPixelSize(w, h);
		bOutput.setPixelSize(w, h);
		bSelector.setPixelSize(w, h);
		bAction.setPixelSize(w, h);
		bContainer.setPixelSize(w, h);

		w = W / 2;

		vpMain.setPixelSize(w, 735 + 6);
		hpMenu.setPixelSize(w, h + 4);
		hpFileEdition.setPixelSize(90, h);
		hpEdition.setPixelSize(270, h);
		hpControl.setPixelSize(225, h);
		canvas.setPixelSize(W, H);
		scroller.setPixelSize(W / 2, H / 4);
		vp.setPixelSize(w, 2);
		vp.setBorderWidth(1);

		bDel.setTitle("Delete (DEL)");
		bCop.setTitle("Copy (C)");
		bCut.setTitle("Cut (X)");
		bPas.setTitle("Paste (V)");
		bEdit.setTitle("Edit AIU (E)");
		bExpand.setTitle("Collapse/Expand AIU container (R)");
		bUp.setTitle("Collapse all containers (L)");
		bDown.setTitle("Expand all containers (M)");
		bOption.setTitle("Options");

		bInput.setTitle("Insert Input AIU (NUMPAD 1)");
		bOutput.setTitle("Insert Output (NUMPAD 2)");
		bSelector.setTitle("Insert Selection AIU (NUMPAD 3)");
		bAction.setTitle("Insert Action AIU (NUMPAD 4)");
		bContainer.setTitle("Insert Container AIU (NUMPAD 0)");

		bOpen.setTitle("Open task model (O)");
		bLoad.setTitle("Open AUI model (A)");
		bNew.setTitle("New (N)");
		bSave.setTitle("Save (S)");
		bSelection.setTitle("Adaption based on Task model structure (F)");
	}

	/**
	 * Reset the add AIU button to their unselected status </br> basic image
	 */
	private void resetButtonIcons() {
		int w = 32, h = 32;
		if (iButtonIcon == 0) {
			hpControl.remove(iButtonIcon);
			bInput = new PushButton(new Image(GWT.getModuleBaseURL()
					+ "images/input.png"));
			bInput.setPixelSize(w, h);
			bInput.setTitle("Input");
			bInput.addClickListener(this);
			hpControl.insert(bInput, iButtonIcon);
		}
		if (iButtonIcon == 1) {
			hpControl.remove(iButtonIcon);
			bOutput = new PushButton(new Image(GWT.getModuleBaseURL()
					+ "images/output.png"));
			bOutput.setPixelSize(w, h);
			bOutput.setTitle("Output");
			bOutput.addClickListener(this);
			hpControl.insert(bOutput, iButtonIcon);
		}
		if (iButtonIcon == 2) {
			hpControl.remove(iButtonIcon);
			bSelector = new PushButton(new Image(GWT.getModuleBaseURL()
					+ "images/selection.png"));
			bSelector.setPixelSize(w, h);
			bSelector.setTitle("Selection");
			bSelector.addClickListener(this);
			hpControl.insert(bSelector, iButtonIcon);
		}
		if (iButtonIcon == 3) {
			hpControl.remove(iButtonIcon);
			bAction = new PushButton(new Image(GWT.getModuleBaseURL()
					+ "images/action.png"));
			bAction.setPixelSize(w, h);
			bAction.setTitle("Action");
			bAction.addClickListener(this);
			hpControl.insert(bAction, iButtonIcon);
		}
		if (iButtonIcon == 4) {
			hpControl.remove(iButtonIcon);
			bContainer = new PushButton(new Image(GWT.getModuleBaseURL()
					+ "images/container.png"));
			bContainer.setPixelSize(w, h);
			bContainer.setTitle("Container");
			bContainer.addClickListener(this);
			hpControl.insert(bContainer, iButtonIcon);
		}
	}

	/**
	 * set the button of the current selected addition mode down the button is
	 * determined with the value of {@link AEditor#iButtonIcon}
	 */
	private void setButtonIcons_Down() {
		int w = 32, h = 32;
		for (int i = 0; i < abstractIUnits.size(); i++)
			abstractIUnits.elementAt(i).setSelected(false);
		drawAUIModel();
		if (iButtonIcon == 0) {
			hpControl.remove(iButtonIcon);
			bInput = new PushButton(new Image(GWT.getModuleBaseURL()
					+ "images/input_down.png"));
			bInput.setPixelSize(w, h);
			bInput.setTitle("Input");
			bInput.addClickListener(this);
			hpControl.insert(bInput, iButtonIcon);
		}
		if (iButtonIcon == 1) {
			hpControl.remove(iButtonIcon);
			bOutput = new PushButton(new Image(GWT.getModuleBaseURL()
					+ "images/output_down.png"));
			bOutput.setPixelSize(w, h);
			bOutput.setTitle("Output");
			bOutput.addClickListener(this);
			hpControl.insert(bOutput, iButtonIcon);
		}
		if (iButtonIcon == 2) {
			hpControl.remove(iButtonIcon);
			bSelector = new PushButton(new Image(GWT.getModuleBaseURL()
					+ "images/selection_down.png"));
			bSelector.setPixelSize(w, h);
			bSelector.setTitle("Selection");
			bSelector.addClickListener(this);
			hpControl.insert(bSelector, iButtonIcon);
		}
		if (iButtonIcon == 3) {
			hpControl.remove(iButtonIcon);
			bAction = new PushButton(new Image(GWT.getModuleBaseURL()
					+ "images/action_down.png"));
			bAction.setPixelSize(w, h);
			bAction.setTitle("Action");
			bAction.addClickListener(this);
			hpControl.insert(bAction, iButtonIcon);
		}
		if (iButtonIcon == 4) {
			hpControl.remove(iButtonIcon);
			bContainer = new PushButton(new Image(GWT.getModuleBaseURL()
					+ "images/container_down.png"));
			bContainer.setPixelSize(w, h);
			bContainer.setTitle("Container");
			bContainer.addClickListener(this);
			hpControl.insert(bContainer, iButtonIcon);
		}
	}

	/**
	 * Add shortcut support the {@link AEditor} </br> ESCAPE key: go out AIU
	 * addition mode set {@link AEditor#iButtonIcon} to -1 </br> O key: upload
	 * task model</br> N key: new AUI model</br> F key: filter previously loaded
	 * task model</br> A key: upload AUI model</br> S key: save/download current
	 * AUI model</br> C key: copy currently selected AIU(s)</br> X key: cut
	 * currently selected AIU(s)</br> V key: paste cut/copied AIU(s)</br> E key:
	 * edit the selected AIU</br> R key: expand/collapse the selected AIU
	 * container(s)</br> L key: collapse all AIU containers</br> M key: expand
	 * all AIU containers</br>
	 */
	protected void onPreviewNativeEvent(NativePreviewEvent event) {
		super.onPreviewNativeEvent(event);
		switch (event.getTypeInt()) {
		case Event.ONKEYDOWN:
			//AIU addition mode
			if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_NUM_ONE) {
				resetButtonIcons();
				if (iButtonIcon == 0)
					iButtonIcon = -1;
				else {
					iButtonIcon = 0;
					setButtonIcons_Down();
				}
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_NUM_TWO) {
				resetButtonIcons();
				if (iButtonIcon == 1)
					iButtonIcon = -1;
				else {
					iButtonIcon = 1;
					setButtonIcons_Down();
				}
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_NUM_THREE) {
				resetButtonIcons();
				if (iButtonIcon == 2)
					iButtonIcon = -1;
				else {
					iButtonIcon = 2;
					setButtonIcons_Down();
				}
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_NUM_FOUR) {
				resetButtonIcons();
				if (iButtonIcon == 3)
					iButtonIcon = -1;
				else {
					iButtonIcon = 3;
					setButtonIcons_Down();
				}
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_NUM_ZERO) {
				resetButtonIcons();
				if (iButtonIcon == 4)
					iButtonIcon = -1;
				else {
					iButtonIcon = 4;
					setButtonIcons_Down();
				}
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ESCAPE) {
				resetButtonIcons();
				iButtonIcon = -1;
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_O) {
				resetButtonIcons();
				iButtonIcon = -1;
				new OpenTaskModel("Open task model", this).show();
				return;
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_N) {
				resetButtonIcons();
				iButtonIcon = -1;
				if (abstractIUnits.size() == 0) {
					newAUIModel(-1);
					return;
				}
				new MyYesNoDialog("Confirmation",
						"Do you want to save this model", this, "save").show();
				return;
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_F) {
				resetButtonIcons();
				iButtonIcon = -1;
				if (getTasks().size() != 0) {
					new AUIGeneratorDialog("Tasks model to AUI model", this)
							.show();
					return;
				} else {
					new MyMessageDialog("Warning",
							"You must load a Task file before.").show();
					return;
				}
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_A) {
				resetButtonIcons();
				iButtonIcon = -1;
				new OpenAuiXml("Open AUI model", this).show();
				return;
			} else if (abstractIUnits.size() > 0) { // if there is AIU in the
													// AUI model
				if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_S) {
					resetButtonIcons();
					iButtonIcon = -1;
					new MySaveDialog(this).show();
					return;
				} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_C) {
					resetButtonIcons();
					iButtonIcon = -1;
					if (selectedAIUhasChildren()) // if the selected AUI
													// container have children
						new MyYesNoDialog(
								"Copy AIUs",
								"Do you want to copy the children of the selected objects?",
								this, "copy").show();
					else
						copyAndCutAUIs(false);
					if (copiedAIUnits.size() == 0) // if no element were copied
						return;
					setCopyMode(); // set isCopy true and isCut false
					return;
				} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_X) {
					resetButtonIcons();
					iButtonIcon = -1;
					if (selectedAIUhasChildren()) // if the selected AUI
													// container have children
						new MyYesNoDialog(
								"Cut AIUs",
								"Do you want to cut the children of the selected objects?",
								this, "cut").show();
					else
						copyAndCutAUIs(false);
					if (copiedAIUnits.size() == 0) // if no element were cut
						return;
					setCutMode(); // set isCut true and isCopy false
					return;
				} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_V) {
					resetButtonIcons();
					iButtonIcon = -1;
					if (copiedAIUnits.size() == 0) // if no element were copied
						return;
					if (getNumofSelectedAUIs() != 1) { // paste cannot be
														// applied on multiple
														// AIU selection
						new MyMessageDialog("Warning",
								"Select one AIU, please!").show();
						return;
					}

					for (int i = 0; i < abstractIUnits.size(); i++)
						if (abstractIUnits.elementAt(i).isSelected()) {
							if (abstractIUnits.elementAt(i).getId() == 0) {
								paste(1); // inside root
								return;
							}
							if (isCut) // cut if isCut is true
								for (int j = 0; j < copiedAIUnits.size(); j++)
									if (abstractIUnits.elementAt(i).getId() == copiedAIUnits
											.elementAt(j).getId()) {
										new MyMessageDialog("Warning",
												"Selected AIU is in the list of cuted AIUs, please select another one!")
												.show();
										return;
									}
							// pasteDialog is the selected AIU is a container
							new MyPasteDialog("Paste dialog",
									abstractIUnits.elementAt(i).getType()
											.indexOf("Contain") >= 0 ? true
											: false, this).show();
						}
					return;
				} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_E) {
					resetButtonIcons();
					iButtonIcon = -1;
					editAIU();
					return;
				} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_R) {
					resetButtonIcons();
					iButtonIcon = -1;
					switchExpanded();
					return;

				} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_L) {
					resetButtonIcons();
					iButtonIcon = -1;
					// Collapse all AIUs
					for (int j = 0; j < abstractIUnits.size(); j++) {
						if (abstractIUnits.elementAt(j).getType()
								.indexOf("Contain") >= 0
								&& abstractIUnits.elementAt(j).getId() > 0)
							abstractIUnits.elementAt(j).setExpanded(false);
					}
					setDimension();
					drawAUIModel();
					return;
				} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_M) {
					resetButtonIcons();
					iButtonIcon = -1;
					// Expand all AIUs
					for (int j = 0; j < abstractIUnits.size(); j++) {
						abstractIUnits.elementAt(j).setExpanded(true);
					}
					setDimension();
					drawAUIModel();
					return;
				} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_DELETE) {
					resetButtonIcons();
					iButtonIcon = -1;
					if (getNumofSelectedAUIs() > 0) {
						if (abstractIUnits.size() == 0)
							return;
						if (abstractIUnits.elementAt(0).isSelected()) {
							new MyMessageDialog("Warning",
									"You can not delete the main contain.")
									.show();
							return;
						}
						if (selectedAIUhasChildren()) // selected AIU have
														// children ask if their
														// should be deleted
							new MyYesNoDialog(
									"Delete AIUs",
									"Do you want to delete the children of the selected objects?",
									this, "delete").show();
						else
							deleteSelectedAUI(2);// delete only selected. Don't
													// need
													// to ask user if he wants
													// to
													// delete the AUI child
					}
					return;
				}
			}
			break;
		}
	}

	/**
	 * set {@link AEditor#isCtrlPressing} to false when CTRL is released
	 */
	public void onKeyUp(Widget sender, char keyCode, int modifiers) {
		if (keyCode == KeyboardListener.KEY_CTRL) {
			isCtrlPressing = false;
		}
	}

	public void onKeyPress(Widget sender, char keyCode, int modifiers) {

	}

	/**
	 * set {@link AEditor#isCtrlPressing} to true when CTRL is pressed down
	 */
	public void onKeyDown(Widget sender, char keyCode, int modifiers) {
		if (keyCode == KeyboardListener.KEY_CTRL) {
			isCtrlPressing = true;
		}
	}

	/**
	 * perform the action related with the pressed button
	 */
	public void onClick(Widget sender) {
		// go to AIU addition mode
		if (iButtonIcon >= 0)
			resetButtonIcons();
		if (sender.equals(bInput)) {
			if (iButtonIcon == 0)
				iButtonIcon = -1;
			else {
				iButtonIcon = 0;
				setButtonIcons_Down();
			}
		} else if (sender.equals(bOutput)) {
			if (iButtonIcon == 1)
				iButtonIcon = -1;
			else {
				iButtonIcon = 1;
				setButtonIcons_Down();
			}
		} else if (sender.equals(bSelector)) {
			if (iButtonIcon == 2)
				iButtonIcon = -1;
			else {
				iButtonIcon = 2;
				setButtonIcons_Down();
			}
		} else if (sender.equals(bAction)) {
			if (iButtonIcon == 3)
				iButtonIcon = -1;
			else {
				iButtonIcon = 3;
				setButtonIcons_Down();
			}
		} else if (sender.equals(bContainer)) {
			if (iButtonIcon == 4)
				iButtonIcon = -1;
			else {
				iButtonIcon = 4;
				setButtonIcons_Down();
			}
		} else {
			iButtonIcon = -1;
			if (sender.equals(bOpen)) {
				new OpenTaskModel("Open task model", this).show();
				return;
			} else if (sender.equals(bLoad)) {
				new OpenAuiXml("Open AUI model", this).show();
				return;
			} else if (sender.equals(bNew)) {
				if (abstractIUnits.size() == 0) {
					newAUIModel(-1);
					return;
				}
				new MyYesNoDialog("Confirmation",
						"Do you want to save this model", this, "save").show();
				return;
			} else if (sender.equals(bOption)) {
				new MyViewOptionDialog(this, columnNumber, Width, Height,
						widthBetweenAIU, heightBetweenAIU).show();
				return;
			} else if (sender.equals(bSelection)) {
				if (getTasks().size() != 0) {
					new AUIGeneratorDialog("Tasks model to AUI model", this)
							.show();
					return;
				} else {
					new MyMessageDialog("Warning",
							"You must load a Task file before.").show();
					return;
				}
			} else if (abstractIUnits.size() > 0) { // if there is AIU in the
													// AUI model
				if (sender.equals(bSave)) {
					new MySaveDialog(this).show();
					return;
				} else if (sender.equals(bCop)) {
					if (selectedAIUhasChildren()) // if the selected AUI
													// container have children
						new MyYesNoDialog(
								"Copy AIUs",
								"Do you want to copy the children of the selected objects?",
								this, "copy").show();
					else
						copyAndCutAUIs(false);
					if (copiedAIUnits.size() == 0) // if no elements were copied
						return;
					setCopyMode(); // set isCopy true and isCut false
					return;
				} else if (sender.equals(bCut)) {
					if (selectedAIUhasChildren()) // if the selected AUI
													// container have childre
						new MyYesNoDialog(
								"Cut AIUs",
								"Do you want to cut the children of the selected objects?",
								this, "cut").show();
					else
						copyAndCutAUIs(false);
					if (copiedAIUnits.size() == 0) // if no elements were cut
						return;
					setCutMode(); // set isCut true and isCopy false
					return;
				} else if (sender.equals(bPas)) {
					if (copiedAIUnits.size() == 0) // if no elements were copied
						return;
					if (getNumofSelectedAUIs() != 1) { // paste cannot be
														// applied on multiple
														// AIU selection
						new MyMessageDialog("Warning",
								"Select one AIU, please!").show();
						return;
					}

					for (int i = 0; i < abstractIUnits.size(); i++)
						if (abstractIUnits.elementAt(i).isSelected()) {
							if (abstractIUnits.elementAt(i).getId() == 0) {
								paste(1); // inside root
								return;
							}
							if (isCut) // cut if isCut is true
								for (int j = 0; j < copiedAIUnits.size(); j++)
									if (abstractIUnits.elementAt(i).getId() == copiedAIUnits
											.elementAt(j).getId()) {
										new MyMessageDialog("Warning",
												"Selected AIU is in the list of cuted AIUs, please select another one!")
												.show();
										return;
									}
							// pasteDialog if the selected AIU is a container
							new MyPasteDialog("Paste dialog",
									abstractIUnits.elementAt(i).getType()
											.indexOf("Contain") >= 0 ? true
											: false, this).show();
						}

					return;
				} else if (sender.equals(bEdit)) {
					editAIU();
					return;
				} else if (sender.equals(bExpand)) {
					// collapse/expand the selected AIU container
					switchExpanded();
					return;

				} else if (sender.equals(bUp)) {
					// collapse all AIU containers
					for (int j = 0; j < abstractIUnits.size(); j++) {
						if (abstractIUnits.elementAt(j).getType()
								.indexOf("Contain") >= 0
								&& abstractIUnits.elementAt(j).getId() > 0)
							abstractIUnits.elementAt(j).setExpanded(false);
					}
					setDimension();
					drawAUIModel();
					return;
				} else if (sender.equals(bDown)) {
					// expand all AIU containers
					for (int j = 0; j < abstractIUnits.size(); j++) {
						abstractIUnits.elementAt(j).setExpanded(true);
					}
					setDimension();
					drawAUIModel();
					return;
				} else if (sender.equals(bDel)) {
					if (getNumofSelectedAUIs() > 0) {
						if (abstractIUnits.size() == 0)
							return;
						if (abstractIUnits.elementAt(0).isSelected()) {
							new MyMessageDialog("Warning",
									"You can not delete the main contain.")
									.show();
							return;
						}
						if (selectedAIUhasChildren()) // selected AIU have
														// children ask if their
														// should be deleted
							new MyYesNoDialog(
									"Delete AIUs",
									"Do you want to delete the children of the selected objects?",
									this, "delete").show();
						else
							deleteSelectedAUI(2);// delete only selected. Don't
													// need
													// to ask user if he wants
													// to
													// delete the AUI child
					}
					return;
				}
			} else {
				// warning message is no AUI model are present
				new MyMessageDialog("Warning",
						"Generate or Create an AUI first.").show();
				return;
			}
		}

	}

	/**
	 * used with the {@link MyPoint} recorded on the onMouseDown() function to
	 * selected the AUI in the rectangle
	 */
	public void onMouseUp(Widget sender, int x, int y) {
		// ignore if in AIU addition mode
		if (iButtonIcon >= 0)
			return;
		// for selecting AIUs
		if (sender.equals(canvas)) // canvas clicked
		{
			boolean isSelected = false;
			if (!isCtrlPressing) // reset the currect selection if the CTRL key
									// is not pressed
				for (int i = 0; i < abstractIUnits.size(); i++)
					abstractIUnits.elementAt(i).setSelected(false);

			if ((p1.getX() != 0) && (p1.getY() != 0) && (!isCtrlPressing)) {

				p2 = new MyPoint(x, y);
				setSelectedAIUs(p1, p2);

				for (int i = 0; i < abstractIUnits.size(); i++)
					if (abstractIUnits.elementAt(i).isSelected())
						isSelected = true;
			}

			if (!isSelected) {

				AbstractIUnit selectedAUIObject = getSelectedUI(x, y);

				if (selectedAUIObject != null) {

					selectedAUIObject.setSelected(selectedAUIObject
							.isSelected() ? false : true);

				} else {
					for (int i = 0; i < abstractIUnits.size(); i++)
						abstractIUnits.elementAt(i).setSelected(false);
				}
			}
			drawAUIModel();
		}
		p1 = p2 = new MyPoint(0, 0);
	}

	/**
	 * if in AIU addition mode add the AIU in the AIU container where the mouse
	 * is, if in normal mode save the coordinate in a {@link MyPoint} for
	 * selection when the mouse is released
	 */
	public void onMouseDown(Widget sender, int x, int y) {
		p1 = p2 = new MyPoint(0, 0);
		if (sender.equals(canvas)) // canvas clicked
		{
			// -1: no button pressed; 0: Input; 1: Output; 2: selection; 3:
			// Action; 4 container
			if (iButtonIcon >= 0) {
				String sType = "";
				if (iButtonIcon == 0)
					sType = "input";
				else if (iButtonIcon == 1)
					sType = "output";
				else if (iButtonIcon == 2)
					sType = "select";
				else if (iButtonIcon == 3)
					sType = "action";
				else
					sType = "Contain";
				AbstractIUnit selectedAUIObject = getSelectedUI(x, y);
				if (selectedAUIObject == null)
					return;

				int index = 0;
				for (int i = 0; i < abstractIUnits.size(); i++)
					if (abstractIUnits.elementAt(i).getId() == selectedAUIObject
							.getId()) {
						index = i;
						break;
					}
				// an input or output or action or selection is selected
				// so the position of new object is the position of selected
				// object
				if (selectedAUIObject.getType().indexOf("Contain") < 0)
					new AIUnitDialog(this, index, sType,
							selectedAUIObject.getParentId()).show();
				else // a container is selected
				{
					if (y < selectedAUIObject.getY() + heightBetweenAIU) // add
																			// new
																			// object
																			// to
						// the first
						// position
						new AIUnitDialog(this, index + 1, sType,
								selectedAUIObject.getId()).show();
					else {
						AbstractIUnit newAIU = getPosition(selectedAUIObject,
								x, y);

						if (newAIU == null) // there is no AIU before new
											// AIU (the container is empty or
											// new AIU will be added to the
											// first position)
							new AIUnitDialog(this, index + 1, sType,
									selectedAUIObject.getId()).show();
						else {
							iIndex = 0; // reset iIndex
							getIndexAfterofAIU(newAIU.getId());
							new AIUnitDialog(this, iIndex + 1, sType,
									selectedAUIObject.getId()).show();
						}
					}
				}

			} else
				p1 = new MyPoint(x, y);

		}

	}

	/**
	 * change {@link AEditor#isCut} to false and {@link AEditor#isCopy} to true,
	 * go in copy mode
	 */
	public void setCopyMode() {
		isCut = false;
		isCopy = true;
		drawAUIModel();
	}

	/**
	 * change {@link AEditor#isCut} to true and {@link AEditor#isCopy} to false,
	 * go in cut mode
	 */
	public void setCutMode() {
		isCut = true;
		isCopy = false;
		drawAUIModel();
	}

	/**
	 * Used to {@link AbstractIUnit} after which a new AIU should be added
	 * 
	 * @param aiu
	 *            parent AIU
	 * @param x
	 *            mouse x position
	 * @param y
	 *            mouse y position
	 * @return the {@link AbstractIUnit} after which the a new AIU should be
	 *         added
	 */
	private AbstractIUnit getPosition(AbstractIUnit aiu, int x, int y) {
		AbstractIUnit newO = null;
		int index = -1;
		for (int i = 0; i < abstractIUnits.size(); i++) {
			if (abstractIUnits.elementAt(i).getParentId() == aiu.getId()) {
				if ((y > abstractIUnits.elementAt(i).getY()
						+ abstractIUnits.elementAt(i).getHeight())
						&& (y < abstractIUnits.elementAt(i).getY()
								+ abstractIUnits.elementAt(i).getHeight()
								+ heightBetweenAIU)) {
					index = i;
				}
			}
		}
		if (index < 0)
			for (int i = 0; i < abstractIUnits.size(); i++) {
				if (abstractIUnits.elementAt(i).getParentId() == aiu.getId()) {
					if ((y > abstractIUnits.elementAt(i).getY())
							&& (y < abstractIUnits.elementAt(i).getY()
									+ abstractIUnits.elementAt(i).getHeight())
							&& (x > abstractIUnits.elementAt(i).getX()
									+ abstractIUnits.elementAt(i).getWidth())
							&& (x < abstractIUnits.elementAt(i).getX()
									+ abstractIUnits.elementAt(i).getWidth()
									+ widthBetweenAIU)) {
						index = i;
						break;
					}
				}
			}

		if (index > -1)
			newO = abstractIUnits.elementAt(index);
		return newO;
	}

	/**
	 * insert a new {@link AbstractIUnit} with the information in parameter in
	 * the {@link AEditor#abstractIUnits} list
	 * 
	 * @param name
	 *            name of the new {@link AbstractIUnit}
	 * @param desc
	 *            description of the new {@link AbstractIUnit}
	 * @param index
	 *            index at which the new {@link AbstractIUnit} should be added
	 * @param type
	 *            type of the new {@link AbstractIUnit}
	 * @param parentId
	 *            id of the parent of the new {@link AbstractIUnit}
	 */
	public void addNewAIUToAUI(String name, String desc, int index,
			String type, int parentId) {

		abstractIUnits.insertElementAt(new AbstractIUnit(componentId++, name,
				desc, type, parentId), index);
		setDimension();
		drawAUIModel();
	}

	/**
	 * paste the copied {@link AbstractIUnit} in the
	 * {@link AEditor#copiedAIUnits} list. </br> three modes possible according
	 * to iButton argument value
	 * 
	 * @param iButton
	 *            if 1 pasted inside the selected AIU container, 2 paste before
	 *            the selected AIU, 3 paste after the selected AIU
	 */
	public void paste(int iButton) {
		if (iButton == 0)// cancel
			return;
		// 1: inside; 2: Before; 3: after
		for (int i = 0; i < abstractIUnits.size(); i++)
			if (abstractIUnits.elementAt(i).isSelected()) {
				if (iButton == 1)// inside
				{
					pastAUIs(abstractIUnits.elementAt(i).getId(), i + 1);
					break;
				} else if (iButton == 2)// before
				{
					pastAUIs(abstractIUnits.elementAt(i).getParentId(), i);
					break;
				} else if (iButton == 3)// after
				{
					iIndex = 0;
					getIndexAfterofAIU(abstractIUnits.elementAt(i).getId());
					pastAUIs(abstractIUnits.elementAt(i).getParentId(),
							iIndex + 1);
					break;
				}
			}
		/**
		 * deleted the copied AIUs if in isCut is true
		 */
		if (isCut) {
			deleteCopiedAUI();
			this.resetCutCopy();
			copiedAIUnits.removeAllElements();
		} else
			setCopiedParent();

		setDimension();
		drawAUIModel();
	}

	/**
	 * reset {@link AEditor#isCut} and {@link AEditor#isCopy} to false
	 */
	public void resetCutCopy() {
		this.isCut = false;
		this.isCopy = false;
	}

	/**
	 * get the index of the AIU after the AIU with the id in argument
	 * 
	 * @param id
	 *            id of an {@link AbstractIUnit}
	 */
	private void getIndexAfterofAIU(int id) {

		for (int i = iIndex; i < abstractIUnits.size(); i++)
			if ((abstractIUnits.elementAt(i).getId() == id)
					&& (abstractIUnits.elementAt(i).getType()
							.indexOf("Contain") < 0)) {
				iIndex = i;
				return;

			}
		// get the last child of AUIs who has ID is iID
		for (int i = iIndex; i < abstractIUnits.size(); i++)
			if (abstractIUnits.elementAt(i).getParentId() == id)
				iIndex = i;

		// if this last child has a child then search its children by calling
		// getIndexofAIU(...)
		for (int i = iIndex; i < abstractIUnits.size(); i++)
			if (abstractIUnits.elementAt(iIndex).getId() == abstractIUnits
					.elementAt(i).getParentId())
				getIndexAfterofAIU(abstractIUnits.elementAt(iIndex).getId());
		// else return index the last child
	}

	/**
	 * return if the {@link AbstractIUnit} is in the rectangle draw between the
	 * two {@link MyPoint} in arguments
	 * 
	 * @param aIU
	 *            {@link AbstractIUnit} to check if its in selection
	 * @param p1
	 *            first corner of the rectangle
	 * @param p2
	 *            second corner of the rectangle
	 * @return
	 */
	private boolean isSelectedAIU(AbstractIUnit aIU, MyPoint p1, MyPoint p2) {
		int x1 = aIU.getX();
		int x2 = aIU.getX() + aIU.getWidth();
		int y1 = aIU.getY();
		int y2 = aIU.getY() + aIU.getHeight();

		if ((x1 >= p1.getX()) && (x1 <= p2.getX()) && (y1 >= p1.getY())
				&& (y1 <= p2.getY()))
			return true;

		if ((x1 >= p2.getX()) && (x1 <= p1.getX()) && (y1 >= p2.getY())
				&& (y1 <= p1.getY()))
			return true;

		if ((x1 >= p1.getX()) && (x1 <= p2.getX()) && (y2 >= p1.getY())
				&& (y2 <= p2.getY()))
			return true;
		if ((x1 >= p2.getX()) && (x1 <= p1.getX()) && (y2 >= p2.getY())
				&& (y2 <= p1.getY()))
			return true;

		if ((x2 >= p1.getX()) && (x2 <= p2.getX()) && (y1 >= p1.getY())
				&& (y1 <= p2.getY()))
			return true;

		if ((x2 >= p2.getX()) && (x2 <= p1.getX()) && (y1 >= p2.getY())
				&& (y1 <= p1.getY()))
			return true;

		if ((x2 >= p1.getX()) && (x2 <= p2.getX()) && (y2 >= p1.getY())
				&& (y2 <= p2.getY()))
			return true;

		if ((x2 >= p2.getX()) && (x2 <= p1.getX()) && (y2 >= p2.getY())
				&& (y2 <= p1.getY()))
			return true;
		if ((x1 >= p1.getX()) && (x1 <= p2.getX()) && (y1 >= p2.getY())
				&& (y1 <= p1.getY()))
			return true;

		if ((x1 >= p1.getX()) && (x1 <= p2.getX()) && (y2 >= p2.getY())
				&& (y2 <= p1.getY()))
			return true;
		if ((x2 >= p1.getX()) && (x1 <= p2.getX()) && (y1 >= p2.getY())
				&& (y1 <= p1.getY()))
			return true;

		if ((x2 >= p1.getX()) && (x1 <= p2.getX()) && (y2 >= p2.getY())
				&& (y2 <= p1.getY()))
			return true;

		if ((x1 >= p2.getX()) && (x1 <= p1.getX()) && (y1 >= p1.getY())
				&& (y1 <= p2.getY()))
			return true;

		if ((x1 >= p2.getX()) && (x1 <= p1.getX()) && (y2 >= p1.getY())
				&& (y2 <= p2.getY()))
			return true;
		if ((x2 >= p2.getX()) && (x1 <= p1.getX()) && (y1 >= p1.getY())
				&& (y1 <= p2.getY()))
			return true;

		if ((x2 >= p2.getX()) && (x1 <= p1.getX()) && (y2 >= p1.getY())
				&& (y2 <= p2.getY()))
			return true;
		return false;
	}

	/**
	 * set the {@link AbstractIUnit} in the {@link AEditor#abstractIUnits}
	 * selected if they are in the rectable draw by the two points
	 * 
	 * @param p1
	 *            first corner of the rectangle
	 * @param p2
	 *            second corner of the rectangle
	 */
	private void setSelectedAIUs(MyPoint p1, MyPoint p2) {
		for (int i = 0; i < abstractIUnits.size(); i++)
			if (isSelectedAIU(abstractIUnits.elementAt(i), p1, p2))
				abstractIUnits.elementAt(i).setSelected(true);
	}

	/**
	 * Delete the selected {@link AbstractIUnit} in the
	 * {@link AEditor#abstractIUnits} list
	 * 
	 * @param iButton
	 *            0 no delete, 1 delete selected AIUs and their children, other
	 *            value: delete only the selected AIUs
	 */
	public void deleteSelectedAUI(int iButton) {

		if (iButton == 0) // cancel
			return;
		if (iButton == 1) // delete all
		{// mark all childrent of selected UIs
			for (int i = 0; i < abstractIUnits.size(); i++) {
				if (abstractIUnits.elementAt(i).isSelected())
					selectAllChildren(abstractIUnits.elementAt(i).getId());
			}
		}
		// Delete selected AUIs
		for (int i = 0; i < abstractIUnits.size(); i++) {
			if (abstractIUnits.elementAt(i).isSelected()) {
				for (int j = 0; j < abstractIUnits.size(); j++) {
					if (abstractIUnits.elementAt(j).getParentId() == abstractIUnits
							.elementAt(i).getId())
						abstractIUnits.elementAt(j).setParentID(
								abstractIUnits.elementAt(i).getParentId());
				}

				abstractIUnits.remove(i);
				i--;
			}

		}
		setDimension();
		drawAUIModel();
	}

	/**
	 * Delete the AIU inside {@link AEditor#copiedAIUnits}
	 */
	private void deleteCopiedAUI() {
		for (int n = 0; n < copiedAIUnits.size(); n++) {
			for (int i = 0; i < abstractIUnits.size(); i++) {
				if (abstractIUnits.elementAt(i).getId() == copiedAIUnits
						.elementAt(n).getId()) {
					for (int j = 0; j < abstractIUnits.size(); j++) {
						if (abstractIUnits.elementAt(j).getParentId() == abstractIUnits
								.elementAt(i).getId())
							abstractIUnits.elementAt(j).setParentID(
									abstractIUnits.elementAt(i).getParentId());
					}

					abstractIUnits.remove(i);
					i--;
				}

			}
		}
	}

	/**
	 * set the {@link AbstractIUnit} with parent id egals to the argument id
	 * 
	 * @param id
	 *            a {@link AbstractIUnit} id
	 */
	private void selectAllChildren(int id) {
		for (int i = 0; i < abstractIUnits.size(); i++) {
			if (abstractIUnits.elementAt(i).getParentId() == id) {
				abstractIUnits.elementAt(i).setSelected(true);
				selectAllChildren(abstractIUnits.elementAt(i).getId());

			}
		}
	}

	/**
	 * return if one of the selected {@link AbstractIUnit} have children
	 * 
	 * @return
	 */
	private boolean selectedAIUhasChildren() {
		for (int i = 0; i < abstractIUnits.size(); i++)
			if (abstractIUnits.elementAt(i).isSelected())
				for (int j = 0; j < abstractIUnits.size(); j++)
					if ((abstractIUnits.elementAt(i).getId() == abstractIUnits
							.elementAt(j).getParentId())
							&& (!abstractIUnits.elementAt(j).isSelected())) // he
						// has a
						// child
						// that
						// this
						// child
						// has
						// been
						// not
						// selected
						return true;

		return false;

	}

	/**
	 * return the number of {@link AbstractIUnit} selected in
	 * {@link AEditor#abstractIUnits}
	 * 
	 * @return
	 */
	private int getNumofSelectedAUIs() {
		int n = 0;
		for (int i = 0; i < abstractIUnits.size(); i++)
			if (abstractIUnits.elementAt(i).isSelected())
				n++;
		return n;

	}

	/**
	 * return if the aui in parameter is a leaf AIU or not
	 * 
	 * @param aui
	 * @return
	 */
	private boolean isLeafAui(AbstractIUnit aui) {
		if (aui.getType().contains("output") || aui.getType().contains("input")
				|| aui.getType().contains("action")
				|| aui.getType().contains("select"))
			return true;
		return false;
	}

	/**
	 * switch between expand/collapse state on the current selected
	 * {@link AbstractIUnit}
	 */
	private void switchExpanded() {
		if (abstractIUnits.size() == 0)
			return;
		int n = 0;
		AbstractIUnit selected = null;
		for (int i = 0; i < abstractIUnits.size(); i++)
			if (abstractIUnits.elementAt(i).isSelected()) {
				n++;
				selected = abstractIUnits.elementAt(i);
			}
		if (n == 0) {
			new MyMessageDialog("Warning", "Select at least one item").show();
			return;
		} else if (n > 1) {
			new MyMessageDialog("Warning", "Select only one item").show();
			return;
		} else {
			if (selected.getType().indexOf("Contain") >= 0) {
				selected.switchExpanded();
				setDimension();
				drawAUIModel();
				return;
			} else {
				new MyMessageDialog("Warning", "Select a container").show();
			}
		}
	}

	/**
	 * open the {@link DialogBox} to edit the selected {@link AbstractIUnit}
	 * return warning message none or more than one {@link AbstractIUnit} is
	 * selected
	 */
	private void editAIU() {

		if (abstractIUnits.size() == 0)
			return;

		int n = 0;
		AbstractIUnit selected = null;
		for (int i = 0; i < abstractIUnits.size(); i++)
			if (abstractIUnits.elementAt(i).isSelected()) {
				n++;
				selected = abstractIUnits.elementAt(i);
			}
		if (n == 0) { // no AIU selected
			new MyMessageDialog("Warning", "Select at least one item").show();
			return;
		} else if (n > 1) { // more than one AIU selected
			new MyMessageDialog("Warning", "Select only one item").show();
			return;
		} else {
			if (isLeafAui(selected)) {
				new AIUEdition(this, selected, true).show();
				return;
			} else {
				new AIUEdition(this, selected, false).show();
				return;
			}

		}

	}

	/**
	 * add the selected {@link AbstractIUnit} in {@link AEditor#abstractIUnits}
	 * to {@link AEditor#copiedAIUnits}
	 * 
	 * @param children
	 *            if true add the children of the selected AIUs, if not only the
	 *            selected AIUs
	 */
	public void copyAndCutAUIs(boolean children) {

		// selected the children of the selected AIUs
		if (children) {
			for (int i = 0; i < abstractIUnits.size(); i++) {
				if (abstractIUnits.elementAt(i).isSelected())
					selectAllChildren(abstractIUnits.elementAt(i).getId());
			}
		}
		if (abstractIUnits.size() == 0)
			return;
		// cannot cut the main container
		if ((abstractIUnits.elementAt(0).isSelected()) && (isCut)) {
			new MyMessageDialog("Warning", "You can not cut the main contain.")
					.show();
			return;
		}
		copiedAIUnits.removeAllElements();
		int nbSelected = 0;
		for (int i = 0; i < abstractIUnits.size(); i++) {
			if (abstractIUnits.elementAt(i).isSelected()) {
				copiedAIUnits.add(new AbstractIUnit(abstractIUnits.elementAt(i)
						.getId(), abstractIUnits.elementAt(i).getName(),
						abstractIUnits.elementAt(i).getDescription(),
						abstractIUnits.elementAt(i).getType(), -1));
				nbSelected++;
			}
			setCopiedParent();
		}
		// at least an AIUs should be selected
		if (nbSelected == 0) {
			new MyMessageDialog("Warning", "You must select at least one AIU")
					.show();
		}
	}

	/**
	 * set the parent id to the new {@link AbstractIUnit}s creating while
	 * copying them
	 */
	private void setCopiedParent() {
		for (AbstractIUnit aiuc : copiedAIUnits) {
			aiuc.setParentID(-1);
		}
		for (AbstractIUnit aiuc : copiedAIUnits) {
			for (AbstractIUnit aiu : abstractIUnits) {
				if (aiuc.getId() == aiu.getId()) {
					for (AbstractIUnit aiuc2 : copiedAIUnits) {
						if (aiu.getParentId() == aiuc2.getId()) {
							aiuc.setParentID(aiuc2.getId());
						}
					}
				}
			}
		}
	}

	/**
	 * Paste the copied {@link AbstractIUnit} at the index in
	 * {@link AEditor#abstractIUnits} and inside the container with parent id
	 * 
	 * @param parentId
	 *            id of the container where the element should be pasted
	 * @param index
	 *            index in {@link AEditor#abstractIUnits} where the element
	 *            should be pasted
	 */
	private void pastAUIs(int parentId, int index) {
		int oldId;
		updateParentID(-1, parentId);

		for (int j = 0; j < copiedAIUnits.size(); j++) {

			oldId = copiedAIUnits.elementAt(j).getId();
			abstractIUnits.insertElementAt(new AbstractIUnit(componentId,
					copiedAIUnits.elementAt(j).getName(), copiedAIUnits
							.elementAt(j).getDescription(), copiedAIUnits
							.elementAt(j).getType(), copiedAIUnits.elementAt(j)
							.getParentId()), index);
			index++;
			updateParentID(oldId, componentId++); // update th parent ID of its
													// childrent
		}
	}

	/**
	 * update the parent id of the children of the AIU with its new id
	 * 
	 * @param oldParentID
	 * @param newParentID
	 */
	private void updateParentID(int oldParentID, int newParentID) {
		for (int j = 0; j < copiedAIUnits.size(); j++) {
			if (copiedAIUnits.elementAt(j).getParentId() == oldParentID) {
				copiedAIUnits.elementAt(j).setParentID(newParentID);
			}
		}
	}

	/**
	 * get the {@link AbstractIUnit} at the position x,y in the canvas
	 * 
	 * @param x
	 * @param y
	 * @return
	 */
	private AbstractIUnit getSelectedUI(int x, int y) {
		AbstractIUnit aiu = null;
		for (int i = 0; i < abstractIUnits.size(); i++) {
			if (((abstractIUnits.elementAt(i).getX() <= x) && (x <= abstractIUnits
					.elementAt(i).getX()
					+ abstractIUnits.elementAt(i).getWidth()))
					&& ((abstractIUnits.elementAt(i).getY() <= y) && (y <= abstractIUnits
							.elementAt(i).getY()
							+ abstractIUnits.elementAt(i).getHeight()))) {
				aiu = abstractIUnits.elementAt(i);
			}

		}
		return aiu;
	}

	/**
	 * initialize the imagine used in the canvas to represent AIUs
	 */
	private void initImages() {
		imgSelect = new Image(GWT.getModuleBaseURL()
				+ "images/selection_icon.png");
		imgInput = new Image(GWT.getModuleBaseURL() + "images/input_icon.png");
		imgAction = new Image(GWT.getModuleBaseURL() + "images/action_icon.png");
		imgUp = new Image(GWT.getModuleBaseURL() + "images/up_icon.png");
		imgDown = new Image(GWT.getModuleBaseURL() + "images/down_icon.png");

	}

	/**
	 * return if the parent of the {@link AbstractIUnit} in argument is expanded
	 * 
	 * @param aIUnit
	 * @return
	 */
	public boolean isParentExpanded(AbstractIUnit aIUnit) {
		AbstractIUnit CurrentAIU = aIUnit;
		boolean isExpanded = true;
		while (CurrentAIU.getParentId() >= 0 && isExpanded) {
			for (int i = 0; i < abstractIUnits.size(); i++) {
				if (abstractIUnits.elementAt(i).getId() == CurrentAIU
						.getParentId()) {
					CurrentAIU = abstractIUnits.elementAt(i);
					isExpanded = CurrentAIU.isExpanded();
				}

			}
		}
		return isExpanded;
	}

	/**
	 * drow the current AUI model based on the {@link AbstractIUnit} in
	 * {@link AEditor#abstractIUnits}
	 */
	public void drawAUIModel() {
		context2D.setFillStyle("white");
		context2D.fillRect(0, 0, W, H);
		context2D.setFillStyle("black");
		context2D.setFont("10pt serif");// SansSerif Times New Roman

		String sName = "";

		for (int i = 0; i < abstractIUnits.size(); i++) {
			if (abstractIUnits.elementAt(i).getX() > 0) {
				// draw if the parent AIU if expanded if not do not draw it
				if (isParentExpanded(abstractIUnits.elementAt(i))) {
					context2D.setStrokeStyle("black");
					context2D.setFillStyle("blue");
					context2D.setLineWidth(2);
					// color the copied or cut AIUs
					for (int j = 0; j < copiedAIUnits.size(); j++)
						if (abstractIUnits.elementAt(i).getId() == copiedAIUnits
								.elementAt(j).getId()) {
							if (isCut) {
								context2D.setStrokeStyle("Gray");
								context2D.setFillStyle("Gray");
							} else if (isCopy) {
								context2D.setStrokeStyle("Green");
								context2D.setFillStyle("Green");
							}
						}
					// color the selection AIUs
					if (abstractIUnits.elementAt(i).isSelected()) {
						context2D.setStrokeStyle("red");
						context2D.setFillStyle("red");

					}
					sName = abstractIUnits.elementAt(i).getName();
					if (abstractIUnits.elementAt(i).getType()
							.indexOf("Contain") >= 0) {
						context2D.strokeRect(
								abstractIUnits.elementAt(i).getX(),
								abstractIUnits.elementAt(i).getY(),
								abstractIUnits.elementAt(i).getWidth(),
								abstractIUnits.elementAt(i).getHeight());
						Image img = imgUp;
						// change the image of the container if is expanded
						if (abstractIUnits.elementAt(i).isExpanded())
							img = imgDown;
						context2D.drawImage(ImageElement.as(img.getElement()),
								abstractIUnits.elementAt(i).getX()
										+ abstractIUnits.elementAt(i)
												.getWidth() - 16,
								abstractIUnits.elementAt(i).getY());
						sName = sName.substring(0, sName.length() > 12 ? 12
								: sName.length());
						context2D.fillText(sName, abstractIUnits.elementAt(i)
								.getX() + 3,
								abstractIUnits.elementAt(i).getY() + 15);
					} else {
						if (abstractIUnits.elementAt(i).getType().toLowerCase()
								.indexOf("select") >= 0) {
							context2D.strokeRect(abstractIUnits.elementAt(i)
									.getX(),
									abstractIUnits.elementAt(i).getY(),
									abstractIUnits.elementAt(i).getWidth(),
									abstractIUnits.elementAt(i).getHeight());
							context2D.drawImage(
									ImageElement.as(imgSelect.getElement()),
									abstractIUnits.elementAt(i).getX()
											+ abstractIUnits.elementAt(i)
													.getWidth() - 16,
									abstractIUnits.elementAt(i).getY());
							sName = sName.substring(0, sName.length() > 10 ? 10
									: sName.length());
							context2D.fillText(sName,
									abstractIUnits.elementAt(i).getX() + 5,
									abstractIUnits.elementAt(i).getY() + 15);
						} else if (abstractIUnits.elementAt(i).getType()
								.toLowerCase().indexOf("input") >= 0) {
							context2D.strokeRect(abstractIUnits.elementAt(i)
									.getX(),
									abstractIUnits.elementAt(i).getY(),
									abstractIUnits.elementAt(i).getWidth(),
									abstractIUnits.elementAt(i).getHeight());
							context2D.drawImage(
									ImageElement.as(imgInput.getElement()),
									abstractIUnits.elementAt(i).getX()
											+ abstractIUnits.elementAt(i)
													.getWidth() - 16,
									abstractIUnits.elementAt(i).getY());
							sName = sName.substring(0, sName.length() > 10 ? 10
									: sName.length());
							context2D.fillText(sName,
									abstractIUnits.elementAt(i).getX() + 5,
									abstractIUnits.elementAt(i).getY() + 15);
						} else if (abstractIUnits.elementAt(i).getType()
								.toLowerCase().indexOf("output") >= 0) {
							sName = sName.substring(0, sName.length() > 12 ? 12
									: sName.length());
							context2D.fillText(sName,
									abstractIUnits.elementAt(i).getX() + 5,
									abstractIUnits.elementAt(i).getY() + 15);
						} else {
							sName = sName.substring(0, sName.length() > 10 ? 10
									: sName.length());
							context2D.strokeRect(abstractIUnits.elementAt(i)
									.getX(),
									abstractIUnits.elementAt(i).getY(),
									abstractIUnits.elementAt(i).getWidth(),
									abstractIUnits.elementAt(i).getHeight());
							context2D.drawImage(
									ImageElement.as(imgAction.getElement()),
									abstractIUnits.elementAt(i).getX()
											+ abstractIUnits.elementAt(i)
													.getWidth() - 16,
									abstractIUnits.elementAt(i).getY());
							context2D.fillText(sName,
									abstractIUnits.elementAt(i).getX() + 5,
									abstractIUnits.elementAt(i).getY() + 15);
						}
					}

				} else {
					// if the parent AIU is not expanded dont show the AIU
					abstractIUnits.elementAt(i).setX(-1);
					abstractIUnits.elementAt(i).setY(-1);
					abstractIUnits.elementAt(i).setHeight(0);
					abstractIUnits.elementAt(i).setWidth(0);
				}
			}
		}

		// draw the selection rectangle in the canvas
		if ((p1 != null) && (p2.getX() > 0) && (p2.getY() > 0)) {

			context2D.setStrokeStyle("LIGHT GRAY");
			context2D.setFillStyle("LIGHT GRAY");

			context2D.strokeRect(p1.getX(), p1.getY(), p2.getX() - p1.getX(),
					p2.getY() - p1.getY());
		}

	}

	/**
	 * transform the XML representation of a task model into a list of
	 * {@link UsiXMLTask}s </br> fill the {@link AEditor#tasks} list </br>
	 * 
	 * @param fileString
	 *            XML representation of a task model
	 */
	public void readTasks(String fileString) {
		String sFileContext = fileString;
		sFileContext = sFileContext.replaceAll("&lt;", "<");
		sFileContext = sFileContext.replaceAll("&gt;", ">");
		int index;
		int TaskID = -1, ParentTaskID = -1, RightTask = -1;
		String TaskName, sTaskType = "", TemporalizationType = "";
		String line;
		getTasks().removeAllElements();
		while (sFileContext.length() > 5) {
			line = sFileContext.substring(0, sFileContext.indexOf(">") + 1);
			TaskName = "";
			// Root task
			if (line.indexOf("<tasks") > 0) {
				ParentTaskID = -1;
				index = line.indexOf("id=\"");
				if (index > 0)
					TaskID = Integer.parseInt(line.substring(index + 4,
							line.indexOf("\"", index + 5)));
				index = line.indexOf("name=\"");
				if (index > 0)
					TaskName = line.substring(index + 6,
							line.indexOf("\"", index + 7));
				index = line.indexOf("tasktype=\"");
				if (index > 0)
					sTaskType = line.substring(index + 10,
							line.indexOf("\"", index + 11));

				getTasks().add(
						new UsiXMLTask(TaskID, ParentTaskID, RightTask,
								TaskName, sTaskType, " ", TaskName + "TaskID:"
										+ TaskID));
			}
			// other tasks
			if (line.indexOf("<children ") > 0) {
				index = line.indexOf("id=\"");
				if (index > 0)
					TaskID = Integer.parseInt(line.substring(index + 4,
							line.indexOf("\"", index + 5)));
				index = line.indexOf("name=\"");
				if (index > 0)
					TaskName = line.substring(index + 6,
							line.indexOf("\"", index + 7));
				index = line.indexOf("tasktype=\"");
				if (index > 0)
					sTaskType = line.substring(index + 10,
							line.indexOf("\"", index + 11));
				getTasks().add(
						new UsiXMLTask(TaskID, ParentTaskID, RightTask,
								TaskName, sTaskType, " ", TaskName + "TaskID:"
										+ TaskID));
			}
			sFileContext = sFileContext.substring(
					sFileContext.indexOf(">") + 1, sFileContext.length());
			if (sFileContext == null)
				break;
		}
		sFileContext = fileString;
		sFileContext = sFileContext.replaceAll("&lt;", "<");
		sFileContext = sFileContext.replaceAll("&gt;", ">");
		while (sFileContext.length() > 5) {
			line = sFileContext.substring(0, sFileContext.indexOf(">") + 1);
			RightTask = -1;
			if (line.indexOf("<tasks") > 0) {
				index = line.indexOf("id=\"");
				if (index > 0)
					ParentTaskID = Integer.parseInt(line.substring(index + 4,
							line.indexOf("\"", index + 5)));
			} else if (line.indexOf("<children ") > 0) {
				index = line.indexOf("id=\"");
				if (index > 0)
					ParentTaskID = Integer.parseInt(line.substring(index + 4,
							line.indexOf("\"", index + 5)));
			}
			TaskID = -1;

			if (line.indexOf("<temporalizations ") > 0) {
				while (line.indexOf("</temporalizations>") < 0) {
					if (line.indexOf("<expressions ") > 0) {
						// get task child ID
						index = line.indexOf(" task=\"");
						if (index > 0) {
							if (TaskID > -1)
								RightTask = Integer.parseInt(line.substring(
										index + 7,
										line.indexOf("\"", index + 8)));
							else
								TaskID = Integer.parseInt(line.substring(
										index + 7,
										line.indexOf("\"", index + 8)));

						}
						index = line.indexOf(" type=\"");
						if (index > 0) {
							TemporalizationType = line.substring(index + 7,
									line.indexOf("\"", index + 8));
							TemporalizationType = TemporalizationType.replace(
									"NONDETERMINISTIC_CHOICE", "Interleaving");
						}
					}
					if (RightTask > -1) {
						for (int i = 0; i < getTasks().size(); i++) {
							if (getTasks().elementAt(i).getTaskID() == TaskID) {
								getTasks().elementAt(i).setParentID(
										ParentTaskID);
								getTasks().elementAt(i).setRightTaskID(
										RightTask);
								getTasks().elementAt(i).setTaskTemporalization(
										TemporalizationType);
								TemporalizationType = "";
							}
						}
						TaskID = RightTask;
						RightTask = -1;
					}

					if (sFileContext.indexOf(">") < 0)
						break;
					if (sFileContext.length() == 0)
						break;
					sFileContext = sFileContext.substring(
							sFileContext.indexOf(">") + 1,
							sFileContext.length());
					if (sFileContext == null)
						break;
					line = sFileContext.substring(0,
							sFileContext.indexOf(">") + 1);
				}
			}
			for (int i = 0; i < getTasks().size(); i++) {
				if (getTasks().elementAt(i).getTaskID() == TaskID) {
					getTasks().elementAt(i).setParentID(ParentTaskID);
					getTasks().elementAt(i).setRightTaskID(RightTask);
					getTasks().elementAt(i).setTaskTemporalization(
							TemporalizationType);
				}
			}
			sFileContext = sFileContext.substring(
					sFileContext.indexOf(">") + 1, sFileContext.length() - 1);
			if (sFileContext == null)
				break;

		}
		// set weight to the tasks
		setWeight();
		// if tasks were loaded activate the adaptation methods button
		if (!tasks.isEmpty()) {
			hpFileEdition.remove(1);
			bSelection = new PushButton(new Image(GWT.getModuleBaseURL()
					+ "images/select.png"));
			bSelection.setPixelSize(32, 32);
			bSelection.setTitle("Adaption based on Task model structure");
			bSelection.addClickListener(this);
			hpFileEdition.insert(bSelection, 1);
		}
	}

	/**
	 * set mental workload to the {@link UsiXMLTask} in {@link AEditor#tasks}
	 * 
	 * @param actionTaskTypes
	 */
	private void setMentalWorkLoad(HashMap<String[], Integer> actionTaskTypes) {
		// set task mental workload based on task type found in actionTaskTypes
		// HashMap
		for (int i = 0; i < getTasks().size(); i++) {
			if (isLeafTask(getTasks().elementAt(i))) {
				java.util.Iterator<String[]> it = actionTaskTypes.keySet()
						.iterator();
				while (it.hasNext()) {
					String[] stringList = it.next();
					for (int j = 0; j < stringList.length; j++) {
						if (getTasks().elementAt(i).getTaskName().toLowerCase()
								.indexOf(stringList[j]) >= 0) {
							getTasks().elementAt(i).setMentalWorload(
									actionTaskTypes.get(stringList));
						}
					}
				}
			}
		}
		// set mental workload to the containers according to the children tasks
		// mental workload
		for (int i = getTasks().size() - 1; i >= 0; i--) {
			if (!isLeafTask(getTasks().elementAt(i))) {
				for (int j = 0; j < getTasks().size(); j++) {
					if (getTasks().elementAt(j).getParentID() == getTasks()
							.elementAt(i).getTaskID())
						getTasks().elementAt(i).setMentalWorload(
								getTasks().elementAt(i).getMentalWorload()
										+ getTasks().elementAt(j)
												.getMentalWorload());
				}
			}
		}
	}

	/**
	 * set weight to the {@link UsiXMLTask} in {@link AEditor#tasks}
	 */
	private void setWeight() {
		// set task weight
		for (int i = 0; i < getTasks().size(); i++) {
			if (isLeafTask(getTasks().elementAt(i))) {
				if (getTasks().elementAt(i).getTaskType().indexOf("input") >= 0)
					getTasks().elementAt(i).setWeight(2);
				else if (getTasks().elementAt(i).getTaskType()
						.indexOf("select") >= 0)
					getTasks().elementAt(i).setWeight(3);
				else
					getTasks().elementAt(i).setWeight(1);
			}
		}
		// set task container weight according to their children tasks weight
		for (int i = getTasks().size() - 1; i >= 0; i--) {
			if (!isLeafTask(getTasks().elementAt(i))) {
				for (int j = 0; j < getTasks().size(); j++) {
					if (getTasks().elementAt(j).getParentID() == getTasks()
							.elementAt(i).getTaskID())
						getTasks().elementAt(i).setWeight(
								getTasks().elementAt(i).getWeight()
										+ getTasks().elementAt(j).getWeight());
				}
			}
		}
	}

	/**
	 * set the dimension of the {@link AbstractIUnit} </br> especially the size
	 * of the AUI container which depend of the number of AUI inside it
	 */
	private void setDimension() {
		if (abstractIUnits.size() == 0)
			return;
		int level = 0, CurrentY = Yo, currentX = 0;
		AbstractIUnit aui;
		int x = 0, y = 0, iColNo = 0;
		;
		// reset X,Y, Width, Height for all of objects
		for (int j = 0; j < abstractIUnits.size(); j++) {
			abstractIUnits.elementAt(j).setX(0);
			abstractIUnits.elementAt(j).setY(0);
			abstractIUnits.elementAt(j).setHeight(0);
			abstractIUnits.elementAt(j).setWidth(0);
		}
		// SET ROOT
		abstractIUnits.elementAt(0).setX(Xo);
		abstractIUnits.elementAt(0).setY(Yo);
		abstractIUnits.elementAt(0).setHeight(Height);
		abstractIUnits.elementAt(0).setWidth(Width);
		for (int i = 1; i < abstractIUnits.size(); i++) {
			level = getContainLevel(abstractIUnits.elementAt(i).getParentId());
			if ((abstractIUnits.elementAt(i).getType().indexOf("Contain") >= 0)) {
				abstractIUnits.elementAt(i).setX(Xo + widthBetweenAIU * level);
				aui = getPreviousObject(abstractIUnits.elementAt(i));
				if (aui == null) {
					CurrentY = abstractIUnits.elementAt(i - 1).getY();
				} else
					CurrentY = aui.getY() + aui.getHeight();
				abstractIUnits.elementAt(i).setY(CurrentY + heightBetweenAIU);
				abstractIUnits.elementAt(i).setHeight(Height);
				abstractIUnits.elementAt(i).setWidth(Width);
				resetWHofParents(abstractIUnits.elementAt(i), heightBetweenAIU,
						widthBetweenAIU);
			} else {
				if (level < getContainLevel(abstractIUnits.elementAt(i - 1)
						.getId())) {
					x = 0;
					y = 0;
					iColNo = 0;
					currentX = Xo + level * widthBetweenAIU;
					aui = getPreviousObject(abstractIUnits.elementAt(i));
					if (aui == null) {
						CurrentY = abstractIUnits.elementAt(i - 1).getY();
					} else
						CurrentY = aui.getY() + aui.getHeight();

					for (int j = i; j < abstractIUnits.size(); j++) {
						if ((abstractIUnits.elementAt(j).getType()
								.indexOf("Contain") < 0)
								&& (abstractIUnits.elementAt(j).getParentId() == abstractIUnits
										.elementAt(i).getParentId())) {
							abstractIUnits.elementAt(j).setX(currentX);
							abstractIUnits.elementAt(j).setY(
									CurrentY + heightBetweenAIU);
							abstractIUnits.elementAt(j).setHeight(Height);
							abstractIUnits.elementAt(j).setWidth(Width);
							currentX = currentX + Width + widthBetweenAIU;

							iColNo++;
							x = x + Width + widthBetweenAIU;
							if ((columnNumber != -1)
									&& (x > (Width + widthBetweenAIU)
											* columnNumber))
								x = (Width + widthBetweenAIU) * columnNumber;
							if (iColNo == 1)
								y = y + heightBetweenAIU + Height;
							if (j == abstractIUnits.size() - 1) {
								i = j;
								break;
							}
							if (iColNo == columnNumber) {
								iColNo = 0;
								currentX = Xo + level * widthBetweenAIU;
								CurrentY = CurrentY + heightBetweenAIU + Height;
							}
						} else {
							i = j - 1;
							break;
						}
					}
					// reset its parent
					for (int j = 0; j < abstractIUnits.size(); j++) {
						if (abstractIUnits.elementAt(j).getId() == abstractIUnits
								.elementAt(i).getParentId()) {
							if (abstractIUnits.elementAt(j).isExpanded()) {
								abstractIUnits.elementAt(j).setHeight(
										abstractIUnits.elementAt(j).getHeight()
												+ y);
								if (abstractIUnits.elementAt(j).getWidth() < x
										+ widthBetweenAIU)
									abstractIUnits.elementAt(j).setWidth(
											x + widthBetweenAIU);
							} else {
								abstractIUnits.elementAt(j).setHeight(Height);
								abstractIUnits.elementAt(j).setWidth(Width);
							}
							resetWHofParents(abstractIUnits.elementAt(j),
									heightBetweenAIU, widthBetweenAIU);

							break;
						}
					}
				} else {
					x = 0;
					y = 0;
					iColNo = 0;
					currentX = Xo + level * widthBetweenAIU;
					// get parent.Y
					CurrentY = abstractIUnits.elementAt(i - 1).getY();

					for (int j = i; j < abstractIUnits.size(); j++) {
						if ((abstractIUnits.elementAt(j).getType()
								.indexOf("Contain") < 0)
								&& (abstractIUnits.elementAt(j).getParentId() == abstractIUnits
										.elementAt(i).getParentId())) {

							abstractIUnits.elementAt(j).setX(currentX);
							abstractIUnits.elementAt(j).setY(
									CurrentY + heightBetweenAIU);
							abstractIUnits.elementAt(j).setHeight(Height);
							abstractIUnits.elementAt(j).setWidth(Width);
							currentX = currentX + Width + widthBetweenAIU;

							iColNo++;
							x = x + Width + widthBetweenAIU;
							if ((columnNumber != -1)
									&& (x > (Width + widthBetweenAIU)
											* columnNumber))
								x = (Width + widthBetweenAIU) * columnNumber;

							if (iColNo == 1)
								y = y + heightBetweenAIU + Height;
							if (j == abstractIUnits.size() - 1) {
								i = j;
								break;
							}
							if (iColNo == columnNumber) {
								iColNo = 0;
								currentX = Xo + level * widthBetweenAIU;
								CurrentY = CurrentY + heightBetweenAIU + Height;
							}

						} else {
							i = j - 1;
							break;
						}
					}
					// reset its parent
					for (int j = 0; j < abstractIUnits.size(); j++) {
						if (abstractIUnits.elementAt(j).getId() == abstractIUnits
								.elementAt(i).getParentId()) {
							if (abstractIUnits.elementAt(j).isExpanded()) {
								abstractIUnits.elementAt(j).setHeight(
										y + heightBetweenAIU);
								if (abstractIUnits.elementAt(j).getWidth() < x
										+ widthBetweenAIU)
									abstractIUnits.elementAt(j).setWidth(
											x + widthBetweenAIU);
							} else {
								abstractIUnits.elementAt(j).setHeight(Height);
								abstractIUnits.elementAt(j).setWidth(Width);
							}
							resetWHofParents(abstractIUnits.elementAt(j),
									heightBetweenAIU, widthBetweenAIU);
							break;

						}
					}
				}
			}

		}

	}

	/**
	 * set the width and height of the parent AUI at the position X, Y
	 * 
	 * @param aui
	 *            AUI added
	 * @param Y
	 * @param X
	 */
	private void resetWHofParents(AbstractIUnit aui, int Y, int X) {
		int y, x;
		if (aui.getParentId() == -1)
			return;
		for (int i = 0; i < abstractIUnits.size(); i++) {
			if (aui.getParentId() == abstractIUnits.elementAt(i).getId()) {
				if (abstractIUnits.elementAt(i).isExpanded()) {
					y = aui.getY() - abstractIUnits.elementAt(i).getY()
							+ aui.getHeight() + Y;
					if (y >= abstractIUnits.elementAt(i).getHeight())
						abstractIUnits.elementAt(i).setHeight(y);
					x = aui.getWidth() + X * 2;
					if (x >= abstractIUnits.elementAt(i).getWidth())
						abstractIUnits.elementAt(i).setWidth(x);
				} else {
					abstractIUnits.elementAt(i).setHeight(Height);
					abstractIUnits.elementAt(i).setWidth(Width);
				}
				resetWHofParents(abstractIUnits.elementAt(i), Y, X);
				break;
			}
		}
	}

	/**
	 * get the AUI with the same parent AUI and with the previous index in
	 * {@link AEditor#abstractIUnits}
	 * 
	 * @param aui
	 *            AUI for which the previous AUI is searched
	 * @return
	 */
	private AbstractIUnit getPreviousObject(AbstractIUnit aui) {
		AbstractIUnit aui1 = null;
		for (int i = 0; i < abstractIUnits.size(); i++) {
			if ((aui.getId() != abstractIUnits.elementAt(i).getId())
					&& (aui.getParentId() == abstractIUnits.elementAt(i)
							.getParentId())) {
				if (abstractIUnits.elementAt(i).getX() > 0)
					aui1 = abstractIUnits.elementAt(i);
			}
		}
		return aui1;
	}

	/**
	 * get the depth level at the AUI with the id parentId is.
	 * 
	 * @param parentId
	 * @return
	 */
	private int getContainLevel(int parentId) {
		int level = 0;
		for (int i = 0; i < abstractIUnits.size(); i++) {
			if (parentId == -1)
				return level;
			if (abstractIUnits.elementAt(i).getId() == parentId) {
				parentId = abstractIUnits.elementAt(i).getParentId();
				level++;
				i = -1;
			}
		}
		return level;
	}

	/**
	 * return if the task in argument is the root task of the task model
	 * 
	 * @param uTask
	 * @return
	 */
	private boolean isRoot(UsiXMLTask uTask) {
		for (int i = 0; i < getTasks().size(); i++) {
			if (getTasks().elementAt(i).getTaskID() == uTask.getParentID())
				return false;
		}
		return true;
	}

	/**
	 * Used to adapt the task model into AUI model, several adaptation methods
	 * are available
	 * 
	 * @param filterMode
	 *            adaptation methods </br> 0 platform model adaptation </br> 1
	 *            Limited number of AIU, 2 Limited number of AIU leaf-only </br>
	 *            3 mental workload adaptation method </br> 4 split inputs and
	 *            outputs adaptation method
	 * @param number
	 *            sub options for adaptation method: </br> Platform model: 2 for
	 *            small screen, 3 medium, 4 big </br> Limited number of AIU: 1
	 *            for minimal selection , 2 * number of tasks for maximal
	 *            selection , custom number loaded for custom adaptation </br>
	 *            MentalWorkload adaptation : 1 for beginner, 2 for normal user,
	 *            3 for advanced user, 4 for a professional user <br>
	 * 
	 */
	public void tasksToAUIs(int filterMode, int number) {
		// this function is used to create the groups of AUI objects
		UsiXMLTask rootTask = new UsiXMLTask();
		for (int i = 0; i < getTasks().size(); i++) {
			if (isRoot(getTasks().elementAt(i))) {
				rootTask = getTasks().elementAt(i);
				break;
			}
		}
		// platform model adaptation
		if (filterMode == 0) {
			if ((number == 3) || (number == 2) || (number == 4)) {
				int iGroupWeight = 0, iMaxWeight = 0, iGroupNumber = 0;
				if (number == 2)
					iMaxWeight = TaskAdaptationConstant.highWeight;
				if (number == 3)
					iMaxWeight = TaskAdaptationConstant.mediumWeight;
				if (number == 4)
					iMaxWeight = TaskAdaptationConstant.lowWeight;
				for (int i = 0; i < getTasks().size(); i++) {
					this.getTasks().elementAt(i).setGroupNumber(0);
				}
				for (int i = 0; i < getTasks().size(); i++) {
					iGroupWeight = 0;
					if (getTasks().elementAt(i).getWeight() > 3)
						for (int j = 0; j < getTasks().size(); j++) {
							if (getTasks().elementAt(j).getWeight() < iMaxWeight) {
								if (getTasks().elementAt(i).getTaskID() == getTasks()
										.elementAt(j).getParentID()) {
									if ((iGroupWeight == 0)
											|| (iGroupWeight
													+ getTasks().elementAt(j)
															.getWeight() > iMaxWeight)) {
										// new group inside the container weight
										// exceeded or other container
										iGroupNumber++;
										iGroupWeight = 0;
									}
									getTasks().elementAt(j).setGroupNumber(
											iGroupNumber);
									iGroupWeight = iGroupWeight
											+ getTasks().elementAt(j)
													.getWeight();
								}
							}
						}
				}
			}
			// limited number of AIU
		} else if (filterMode == 1) {

			int maxNb;
			int sizeElement = 0;
			if (number > 1) {
				maxNb = number;
			} else
				maxNb = 1;

			int iGroupSize = 0, iGroupNumber = 0;
			for (int i = 0; i < getTasks().size(); i++) {
				getTasks().elementAt(i).setGroupNumber(0);
			}
			for (int i = 0; i < getTasks().size(); i++) {
				for (int j = 0; j < getTasks().size(); j++) {
					if (getTasks().elementAt(i).getTaskID() == getTasks()
							.elementAt(j).getParentID()) {
						// input and select AIU count as 2 because we add an
						// output AIU later
						if (getTasks().elementAt(j).getTaskType()
								.contains("input")
								|| getTasks().elementAt(j).getTaskType()
										.contains("select"))
							sizeElement = 2;
						else
							sizeElement = 1;
						if (iGroupSize + sizeElement > maxNb) {
							// new group inside the container limit exceeded
							iGroupNumber++;
							iGroupSize = 0;
						}
						getTasks().elementAt(j).setGroupNumber(iGroupNumber);
						iGroupSize = iGroupSize + sizeElement;
					}
				}
				// new group due to other container
				iGroupSize = 0;
				iGroupNumber++;
			}

			// limited number of AIU leaf-task only
		} else if (filterMode == 2) { // Number of element
			// per box select
			// mode = 1
			int maxNb;
			int sizeElement = 0;
			if (number > 1) {
				maxNb = number;
			} else
				maxNb = 1;

			int iGroupSize = 0, iGroupNumber = 0;
			for (int i = 0; i < getTasks().size(); i++) {
				getTasks().elementAt(i).setGroupNumber(0);
			}
			for (int i = 0; i < getTasks().size(); i++) {
				for (int j = 0; j < getTasks().size(); j++) {
					if (getTasks().elementAt(i).getTaskID() == getTasks()
							.elementAt(j).getParentID()) {
						// leaf task only
						if (isLeafTask(getTasks().elementAt(j))) {
							// input and select AIU count as 2 because we add an
							// output AIU later
							if (getTasks().elementAt(j).getTaskType()
									.contains("input")
									|| getTasks().elementAt(j).getTaskType()
											.contains("select"))
								sizeElement = 2;
							else
								sizeElement = 1;
							if (iGroupSize + sizeElement > maxNb) {
								// new group inside the container limit exceeded
								iGroupNumber++;
								iGroupSize = 0;
							}
							getTasks().elementAt(j)
									.setGroupNumber(iGroupNumber);
							iGroupSize = iGroupSize + sizeElement;
						}
					}
				}
				// new group due to other container
				iGroupSize = 0;
				iGroupNumber++;
			}

			// mental workload adaptation method
		} else if (filterMode == 3) {
			TaskAdaptationConstant taskAdaptationConstant = new TaskAdaptationConstant();
			setMentalWorkLoad(taskAdaptationConstant.actionTaskTypes);
			if ((number == 1) || (number == 2) || (number == 3)
					|| (number == 4)) {
				int groupMentalWorkload = 0, maxMentalWorkload = 0, groupNumber = 0;
				if (number == 1)
					maxMentalWorkload = TaskAdaptationConstant.beginner;
				if (number == 2)
					maxMentalWorkload = TaskAdaptationConstant.normal;
				if (number == 3)
					maxMentalWorkload = TaskAdaptationConstant.advanced;
				if (number == 4)
					maxMentalWorkload = TaskAdaptationConstant.professional;
				for (int i = 0; i < getTasks().size(); i++) {
					getTasks().elementAt(i).setGroupNumber(0);
				}
				for (int i = 0; i < getTasks().size(); i++) {
					groupMentalWorkload = 0;
					if (getTasks().elementAt(i).getMentalWorload() > 0)
						for (int j = 0; j < getTasks().size(); j++) {
							if (getTasks().elementAt(j).getMentalWorload() < maxMentalWorkload) {
								if (getTasks().elementAt(i).getTaskID() == getTasks()
										.elementAt(j).getParentID()) {
									if ((groupMentalWorkload == 0)
											|| (groupMentalWorkload
													+ getTasks().elementAt(j)
															.getMentalWorload() > maxMentalWorkload)) {
										// new group mental workload limit
										// exceeded or new container
										groupNumber++;
										groupMentalWorkload = 0;
									}
									getTasks().elementAt(j).setGroupNumber(
											groupNumber);
									groupMentalWorkload = groupMentalWorkload
											+ getTasks().elementAt(j)
													.getMentalWorload();
								}
							}
						}
				}
			}

			// / split outputs and inputs adaptation method
		} else if (filterMode == 4) {
			// type of the previous task
			boolean previousType = true;
			// type of the current task
			boolean currType = true;
			int iGroupNumber = 0;
			for (int i = 0; i < getTasks().size(); i++) {
				getTasks().elementAt(i).setGroupNumber(0);
			}
			// get the type of the task in the task model (input or output)
			boolean isSet = false;
			for (int i = 0; i < getTasks().size(); i++) {
				for (int j = 0; j < getTasks().size(); j++) {
					if (getTasks().elementAt(i).getTaskID() == getTasks()
							.elementAt(j).getParentID()) {
						// leaf task only
						if (isLeafTask(getTasks().elementAt(j))) {
							if (getTasks().elementAt(j).getTaskType()
									.contains("input")
									|| getTasks().elementAt(j).getTaskType()
											.contains("select")
									|| getTasks().elementAt(j).getTaskType()
											.contains("action")) {
								previousType = true;
								isSet = true;
								break;
							} else {
								previousType = false;
								isSet = true;
								break;
							}
						}
					}
				}
				if (isSet)
					break;
			}

			for (int i = 0; i < getTasks().size(); i++) {
				for (int j = 0; j < getTasks().size(); j++) {
					if (getTasks().elementAt(i).getTaskID() == getTasks()
							.elementAt(j).getParentID()) {
						// leaf task only
						if (isLeafTask(getTasks().elementAt(j))) {
							// input task
							if (getTasks().elementAt(j).getTaskType()
									.contains("input")
									|| getTasks().elementAt(j).getTaskType()
											.contains("select")
									|| getTasks().elementAt(j).getTaskType()
											.contains("action"))
								currType = true;
							else
								// output task
								currType = false;
							// if the type of the type is not the same as the
							// type of the previous task
							if (currType != previousType) {
								iGroupNumber++;
							}
							getTasks().elementAt(j)
									.setGroupNumber(iGroupNumber);
							previousType = currType;
						}
					}
				}
				// other AIU container
				iGroupNumber++;
			}
		}
		// reset the AUI model
		abstractIUnits.removeAllElements();
		// non leaf adaptation method
		if (filterMode == 0 || filterMode == 1 || filterMode == 3) {

			componentId = -1;
			AbstractIUnit rootAUIObject = new AbstractIUnit(componentId++, "",
					"", "Contain", -2);
			addContainer_TaskOjectToAUIObject(rootAUIObject, rootTask);
		} else if (filterMode == 2 || filterMode == 4) { // leaf-only adaptation
															// method
			boolean addOutput = true;
			if (filterMode == 4)
				addOutput = false;
			componentId = 0;
			AbstractIUnit rootAUIObject = new AbstractIUnit(componentId++,
					rootTask.getTaskName(), rootTask.getDescription(),
					"Contain", -1);
			abstractIUnits.add(rootAUIObject);
			for (int i = 0; i < getTasks().size(); i++) {
				if (getTasks().elementAt(i).getParentID() == rootTask
						.getTaskID()) {
					addContainer_TaskOjectToAUIObjectLeaf(rootAUIObject,
							getTasks().elementAt(i), addOutput);
				}
			}
		}
		setDimension();
		drawAUIModel();
	}

	/**
	 * add the {@link AbstractIUnit} into {@link AEditor#abstractIUnits} if task
	 * is a leaf-task </br> algorithm used with
	 * {@link AEditor#addContainer_TaskOjectToAUIObject(AbstractIUnit, UsiXMLTask)}
	 * 
	 * @param task
	 *            current task that should be added in the AUI model
	 * @param rootAUIObject
	 *            AUI container in which the leaf AIU should be added
	 */
	private void addGroup_LeafElement(UsiXMLTask task,
			AbstractIUnit rootAUIObject) {
		if (isLeafTask(task)) {
			String type = "";
			if (task.getTaskType().toLowerCase().indexOf("input") >= 0)
				type = "input";
			else if (task.getTaskType().toLowerCase().indexOf("select") >= 0)
				type = "select";
			else if (task.getTaskType().toLowerCase().indexOf("output") >= 0)
				type = "output";
			else if (task.getTaskType().toLowerCase().indexOf("trigger") >= 0)
				type = "action";
			if (task.getTaskType().toLowerCase().indexOf("reserve") >= 0)
				type = "contain";
			if (type.indexOf("action") < 0 && type.indexOf("output") < 0) {
				abstractIUnits.add(new AbstractIUnit(componentId++, task
						.getTaskName(), task.getDescription(), "output",
						rootAUIObject.getId()));
				abstractIUnits.add(new AbstractIUnit(componentId++, task
						.getTaskName(), task.getDescription(), type,
						rootAUIObject.getId()));
			} else
				abstractIUnits.add(new AbstractIUnit(componentId++, task
						.getTaskName(), task.getDescription(), type,
						rootAUIObject.getId()));
		} else {
			addContainer_TaskOjectToAUIObject(rootAUIObject, task);
		}
	}

	/**
	 * add the {@link AbstractIUnit} into {@link AEditor#abstractIUnits} if task
	 * is a leaf-task </br> algorithm used with
	 * {@link AEditor#addContainer_TaskOjectToAUIObjectLeaf(AbstractIUnit, UsiXMLTask, boolean)}
	 * 
	 * @param task
	 *            current task that should be added in the aui model
	 * @param rootAUIObject
	 *            AUI container in which the leaf AIU should be added
	 * @param addOutput
	 *            is true add an output AIU for all input, select tasks
	 */
	private void addGroup_LeafElementLeaf(UsiXMLTask task,
			AbstractIUnit rootAUIObject, boolean addOutput) {
		if (isLeafTask(task)) {
			String type = "";
			if (task.getTaskType().toLowerCase().indexOf("input") >= 0)
				type = "input";
			else if (task.getTaskType().toLowerCase().indexOf("select") >= 0)
				type = "select";
			else if (task.getTaskType().toLowerCase().indexOf("output") >= 0)
				type = "output";
			else if (task.getTaskType().toLowerCase().indexOf("trigger") >= 0)
				type = "action";
			if (task.getTaskType().toLowerCase().indexOf("reserve") >= 0)
				type = "contain";
			if (type.indexOf("action") < 0 && type.indexOf("output") < 0
					&& addOutput) {
				abstractIUnits.add(new AbstractIUnit(componentId++, task
						.getTaskName(), task.getDescription(), "output",
						rootAUIObject.getId()));
				abstractIUnits.add(new AbstractIUnit(componentId++, task
						.getTaskName(), task.getDescription(), type,
						rootAUIObject.getId()));
			} else
				abstractIUnits.add(new AbstractIUnit(componentId++, task
						.getTaskName(), task.getDescription(), type,
						rootAUIObject.getId()));
		} else {
			addContainer_TaskOjectToAUIObjectLeaf(rootAUIObject, task,
					addOutput);
		}
	}

	/**
	 * generated the AUI model starting from the grouped task model from the
	 * leaf-task only
	 * 
	 * @param task
	 *            current task
	 * @param parentAIU
	 *            parent AUI
	 * @param addOutput
	 *            is true add an output AIU for all input, select tasks
	 */
	private void addContainer_TaskOjectToAUIObjectLeaf(AbstractIUnit parentAIU,
			UsiXMLTask task, boolean addOutput) {
		ArrayList<Long> sGroupList = new ArrayList<Long>();
		for (int i = 1; i < getTasks().size(); i++) {
			if (getTasks().elementAt(i).getParentID() == task.getTaskID()) {
				// if the tasks are grouped by developer
				if (getTasks().elementAt(i).getGroupNumber() > 0) {
					if (!sGroupList.contains(getTasks().elementAt(i)
							.getGroupNumber())) {
						sGroupList
								.add(getTasks().elementAt(i).getGroupNumber());

						AbstractIUnit groupContainer = new AbstractIUnit(
								componentId++, "Group"
										+ getTasks().elementAt(i)
												.getGroupNumber(), "Group"
										+ getTasks().elementAt(i)
												.getGroupNumber(), "Contain",
								parentAIU.getId());
						abstractIUnits.add(groupContainer);
						for (int j = 1; j < getTasks().size(); j++) {
							if ((getTasks().elementAt(j).getParentID() == task
									.getTaskID())
									&& getTasks().elementAt(i).getGroupNumber() == getTasks()
											.elementAt(j).getGroupNumber()) {
								addGroup_LeafElementLeaf(getTasks()
										.elementAt(j), groupContainer,
										addOutput);
							}
						}

					}

				} else {
					addGroup_LeafElementLeaf(getTasks().elementAt(i),
							parentAIU, addOutput);
				}
			}
		}

	}

	/**
	 * generated the AUI model starting from the grouped task model
	 * 
	 * @param task
	 *            current task
	 * @param parentAIU
	 *            parent AUI
	 */
	private void addContainer_TaskOjectToAUIObject(AbstractIUnit parentAIU,
			UsiXMLTask task) {
		ArrayList<Long> sGroupList = new ArrayList<Long>();
		long groupValue = 0;
		int oneGroup = 0;

		AbstractIUnit rootAUIObject = new AbstractIUnit(componentId++,
				task.getTaskName(), task.getDescription(), "Contain",
				parentAIU.getId());

		abstractIUnits.add(rootAUIObject);

		for (int i = 1; i < getTasks().size(); i++) {
			if (getTasks().elementAt(i).getParentID() == task.getTaskID()) {
				if (getTasks().elementAt(i).getGroupNumber() > 0
						&& groupValue != getTasks().elementAt(i)
								.getGroupNumber()) {
					groupValue = getTasks().elementAt(i).getGroupNumber();
					oneGroup++;
				}
			}
		}

		for (int i = 1; i < getTasks().size(); i++) {
			if (getTasks().elementAt(i).getParentID() == task.getTaskID()) {
				// if the tasks are grouped by developer
				if (getTasks().elementAt(i).getGroupNumber() > 0) {
					if (!sGroupList.contains(getTasks().elementAt(i)
							.getGroupNumber())) {
						sGroupList
								.add(getTasks().elementAt(i).getGroupNumber());
						boolean isContainer = false;
						int countElement = 0;
						for (int j = 1; j < getTasks().size(); j++) {
							if ((getTasks().elementAt(j).getParentID() == task
									.getTaskID())
									&& getTasks().elementAt(i).getGroupNumber() == getTasks()
											.elementAt(j).getGroupNumber()) {
								countElement++;
								if ((isLeafTask(getTasks().elementAt(j)) && getTasks()
										.elementAt(j).getTaskType()
										.contains("Contain"))
										|| !isLeafTask(getTasks().elementAt(j)))
									isContainer = true;
							}
						}
						if ((oneGroup > 1 && !isContainer && countElement == 1)
								|| (oneGroup > 1 && countElement > 1)) {
							AbstractIUnit groupContainer = new AbstractIUnit(
									componentId++, ""
											+ rootAUIObject.getName()
											+ "Group"
											+ getTasks().elementAt(i)
													.getGroupNumber(), ""
											+ rootAUIObject.getName()
											+ "Group"
											+ getTasks().elementAt(i)
													.getGroupNumber(),
									"Contain", rootAUIObject.getId());
							abstractIUnits.add(groupContainer);
							for (int j = 1; j < getTasks().size(); j++) {
								if ((getTasks().elementAt(j).getParentID() == task
										.getTaskID())
										&& getTasks().elementAt(i)
												.getGroupNumber() == getTasks()
												.elementAt(j).getGroupNumber()) {
									addGroup_LeafElement(getTasks()
											.elementAt(j), groupContainer);
								}
							}
						} else {
							for (int j = 1; j < getTasks().size(); j++) {
								if ((getTasks().elementAt(j).getParentID() == task
										.getTaskID())
										&& getTasks().elementAt(i)
												.getGroupNumber() == getTasks()
												.elementAt(j).getGroupNumber()) {
									addGroup_LeafElement(getTasks()
											.elementAt(j), rootAUIObject);
								}
							}
						}
					}
				} else {
					addGroup_LeafElement(getTasks().elementAt(i), rootAUIObject);
				}
			}
		}

	}

	/**
	 * transform the {@link AbstractIUnit} in {@link AEditor#abstractIUnits}
	 * into an UsiXML representation </br> generate the base of the file, then
	 * we use another function to recursively generate the AIUs.
	 * 
	 * @return
	 */
	private String auiObjectsToUsiXML() {
		sTextFile = "";
		componentIdXml = 0;

		sTextFile = sTextFile + "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
				+ "\n";
		sTextFile = sTextFile
				+ "<org.usixml.aui:AbstractUIModel xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" "
				+ "xmlns:org.usixml.aui=\"http://org.usixml.aui/2.0\">" + "\n";
		sTextFile = sTextFile + "	<compoundIUs id=\"0\" label=\""
				+ abstractIUnits.elementAt(0).getName() + "\" descLabel=\""
				+ abstractIUnits.elementAt(0).getDescription()
				+ "\" dataUIType=\"" + abstractIUnits.elementAt(0).getType()
				+ "\">" + "\n";
		for (int i = 0; i < abstractIUnits.size(); i++) {
			if (abstractIUnits.elementAt(0).getId() == abstractIUnits
					.elementAt(i).getParentId())
				addContainer_AUIObjectsToUsiXmlAUI("	",
						abstractIUnits.elementAt(i));
		}
		sTextFile = sTextFile + "	</compoundIUs>" + "\n"
				+ "</org.usixml.aui:AbstractUIModel>";

		return sTextFile;

	}

	/**
	 * sub function used to transform the {@link AbstractIUnit} in
	 * {@link AEditor#abstractIUnits} into an UsiXML representation
	 */
	private void addContainer_AUIObjectsToUsiXmlAUI(String sSpace,
			AbstractIUnit auiObject) {
		;
		sSpace = sSpace + "	";
		componentIdXml++;
		sTextFile = sTextFile
				+ sSpace
				+ "<interactionUnits xsi:type=\"org.usixml.aui:AbstractCompoundIU\" id=\""
				+ componentIdXml + "\" label=\"" + auiObject.getName()
				+ "\" descLabel=\"" + auiObject.getDescription()
				+ "\" dataUIType=\"" + auiObject.getType() + "\">" + "\n";
		for (int i = 1; i < abstractIUnits.size(); i++) {
			if (abstractIUnits.elementAt(i).getParentId() == auiObject.getId()) {
				if (abstractIUnits.elementAt(i).getType().indexOf("Contain") < 0)// is
																					// a
																					// control
				{
					// <interactionUnits
					// xsi:type="org.usixml.aui:AbstractDataIU" id="18"
					// label="OutStreet" descLabel="Street:" defaultValue=""
					// dataUIType="OUTPUT"/>
					componentIdXml++;
					sTextFile = sTextFile
							+ sSpace
							+ "	<interactionUnits xsi:type=\"org.usixml.aui:AbstractDataIU\" id=\""
							+ componentIdXml++ + "\" label=\""
							+ abstractIUnits.elementAt(i).getName()
							+ "\" descLabel=\""
							+ abstractIUnits.elementAt(i).getDescription()
							+ "\" dataUIType=\""
							+ abstractIUnits.elementAt(i).getType() + "\"/>"
							+ "\n";
				} else {
					addContainer_AUIObjectsToUsiXmlAUI(sSpace,
							abstractIUnits.elementAt(i)); // if it has the
															// children
															// tasks, create a
															// new
															// container by
															// calling
															// AddContainerUI()
				}
			}

		}
		// </interactionUnits>
		sTextFile = sTextFile + sSpace + "</interactionUnits>" + "\n";

	}

	/**
	 * transform the {@link AbstractIUnit} in {@link AEditor#abstractIUnits}
	 * into an W3C AUI meta-model XML representation
	 * 
	 * @return
	 */
	private String auiObjectsToW3CXML() {
		sTextFile = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\n";
		sTextFile = sTextFile
				+ "<ns1:abstractUserInterfaceModel xmlns:uic=\"http://www.w3.org/2013/mbui-uiCommons\""
				+ "\n";
		sTextFile = sTextFile + "xmlns:ns1=\"http://www.w3.org/2013/mbui-aui\""
				+ "\n";

		sTextFile = sTextFile
				+ "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
				+ "\n";
		for (int i = 0; i < abstractIUnits.size(); i++) {
			boolean isCompound = abstractIUnits.elementAt(i).getType()
					.contains("Contain");
			sTextFile = sTextFile + "	<ns1:abstractInteractionUnit id=\""
					+ abstractIUnits.elementAt(i).getId() + "\" name=\""
					+ abstractIUnits.elementAt(i).getName() + "\" role=\""
					+ abstractIUnits.elementAt(i).getType() + "\" compound=\""
					+ isCompound + "\">" + "\n";
			if (isCompound) {
				sTextFile = sTextFile
						+ "		<ns1:aggregatedIn"
						+ " compositionRole=\"default\" compositionRationale=\"default\">"
						+ "\n";
				ArrayList<Integer> childrensID = new ArrayList<Integer>();
				for (int j = 0; j < abstractIUnits.size(); j++) {
					if (abstractIUnits.elementAt(i).getId() == abstractIUnits
							.elementAt(j).getParentId())
						childrensID.add(abstractIUnits.elementAt(j).getId());
				}
				for (int id : childrensID) {
					sTextFile = sTextFile + "			<ns1:associatedWith>" + id
							+ "</ns1:associatedWith>" + "\n";
				}
				sTextFile = sTextFile + "		</ns1:aggregatedIn>" + "\n";

			} else {

				sTextFile = sTextFile + "		<ns1:presentationSupport>" + "\n";
				if (!abstractIUnits.elementAt(i).getType().contains("select"))
					sTextFile = sTextFile + "			<ns1:dataInputOutputSupport/>"
							+ "\n";
				else
					sTextFile = sTextFile + "			<ns1:dataSelectionSupport/>"
							+ "\n";
				sTextFile = sTextFile + "		</ns1:presentationSupport>" + "\n";
			}
			sTextFile = sTextFile + "	</ns1:abstractInteractionUnit>" + "\n";
		}
		return sTextFile;

	}

	/**
	 * Method use save/download the AUI model in {@link AEditor#abstractIUnits},
	 * this method uses the Servlet {@link FileDownloadServlet} to perform the
	 * operation on file
	 * 
	 * @param filename
	 *            name of the AUI model that will be downloaded
	 * @param format
	 *            0 for UsiXML format, 1 for the W3C AUI meta-model XML format
	 */
	public void saveAUIModel(String filename, int format) {
		// field containing the XML representation of the AUI model
		String sAUIContext = "";
		if (format == 0)
			sAUIContext = auiObjectsToUsiXML();
		else if (format == 1)
			sAUIContext = auiObjectsToW3CXML();
		String link = GWT.getModuleBaseURL() + "savefile";
		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, link);
		builder.setHeader("Content-type", "application/x-www-form-urlencoded");

		// POST request to the Servlet used to set the XML representation of the
		// AUI model and the file name
		try {
			builder.sendRequest("fileName=" + filename + "&AUIXML="
					+ sAUIContext, new RequestCallback() {

				public void onError(Request request, Throwable exception) {
				}

				public void onResponseReceived(Request request,
						Response response) {
					// file download window in an hidden IFrame, post request
					// made to download the previously loaded XML
					AUIEditor.downloadURL((GWT.getModuleBaseURL() + "savefile"));
				}
			});
		} catch (RequestException e) {
		}
	}

	/**
	 * Not useful at this moment but can be used to set access control later
	 * Check the identity of the user , not comple
	 * 
	 * @param sUserName
	 * @param sPassword
	 */
	private void verifyUser(String sUserName, String sPassword) {

		greetingService.verifyUser(sUserName, sPassword,
				new AsyncCallback<Boolean>() {
					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user
						Window.alert(caught.getMessage());
					}

					public void onSuccess(Boolean result) {
						// can be used late to add access control to the
						// application
						/*
						 * if (result) // continue Window.alert("Welcome"); else
						 * // ask user login .... Window.alert("login pleas!");
						 */
					}
				});
	}

	/**
	 * get an XML representation of an AUI model and call the correct methods
	 * according to the type of the XML representation
	 * 
	 * @param sTextContent
	 *            XML representation of an AUI model
	 */
	public void xmlToAIUsObject(String sTextContent) {
		if (sTextContent.contains("xmlns:org.usixml.aui")) {
			usiXMLToAIUsObject(sTextContent);
		} else if (sTextContent
				.contains("xmlns:ns1=\"http://www.w3.org/2013/mbui-aui\"")) {
			w3cXMLToAIUSObject(sTextContent);
		}
	}

	/**
	 * Transform an UsiXML representation of a AUI model into
	 * {@link AbstractIUnit} object in the {@link AEditor#abstractIUnits} list
	 * 
	 * @param sTextContent
	 */
	public void usiXMLToAIUsObject(String sTextContent) {
		// this function is used to AUI Objects such as slector, input ...
		// container
		// from AUI text model ----> FROM text in .xml file TO AUI Objects.
		String s, sType, sName, sDesc;
		int id = 0, parentID;
		int maxcomponentID = -1;
		sTextContent = sTextContent.replaceAll("&lt;", "<");
		sTextContent = sTextContent.replaceAll("&gt;", ">");
		abstractIUnits.removeAllElements();
		sTextContent = sTextContent.substring(sTextContent.indexOf("\n") + 1,
				sTextContent.length());
		sTextContent = sTextContent.substring(sTextContent.indexOf("\n") + 1,
				sTextContent.length());
		s = sTextContent.substring(1, sTextContent.indexOf("\n"));
		int index = s.indexOf("label=\"");
		sName = s.substring(index + 7, s.indexOf("\"", index + 8));
		index = s.indexOf("descLabel=\"");
		sDesc = s.substring(index + 11, s.indexOf("\"", index + 12));
		index = s.indexOf("dataUIType=\"");
		sType = s.substring(index + 12, s.indexOf("\"", index + 13));
		index = s.indexOf("id=\"");
		if (index > 0)
			id = Integer.parseInt(s.substring(index + 4,
					s.indexOf("\"", index + 5)));
		abstractIUnits.add(new AbstractIUnit(id, sName, sDesc, sType, -1));
		sTextContent = sTextContent.substring(sTextContent.indexOf("\n") + 1,
				sTextContent.length());
		parentID = id;
		maxcomponentID = Math.max(maxcomponentID, id);

		while (s.length() > 0) {
			sName = "";
			sType = "";

			s = sTextContent.substring(1, sTextContent.indexOf("\n"));
			if (s.indexOf("</compoundIUs>") >= 0)
				break;
			sTextContent = sTextContent.substring(
					sTextContent.indexOf("\n") + 1, sTextContent.length());
			// create AUI Object
			if (s.indexOf("<interactionUnits") >= 0) {
				if (s.indexOf("/>") > 0) // UI control
				{

					index = s.indexOf("label=\"");
					if (index > 0)
						sName = s.substring(index + 7,
								s.indexOf("\"", index + 8));
					index = s.indexOf("descLabel=\"");
					if (index > 0)
						sDesc = s.substring(index + 11,
								s.indexOf("\"", index + 12));
					index = s.indexOf("dataUIType=\"");
					if (index > 0)
						sType = s.substring(index + 12,
								s.indexOf("\"", index + 13));
					index = s.indexOf("id=\"");
					if (index > 0)
						id = Integer.parseInt(s.substring(index + 4,
								s.indexOf("\"", index + 5)));
					abstractIUnits.add(new AbstractIUnit(id, sName, sDesc,
							sType, parentID));
					maxcomponentID = Math.max(maxcomponentID, id);

				} else // UI contain
				{
					index = s.indexOf("label=\"");
					if (index > 0)
						sName = s.substring(index + 7,
								s.indexOf("\"", index + 8));
					index = s.indexOf("descLabel=\"");
					if (index > 0)
						sDesc = s.substring(index + 11,
								s.indexOf("\"", index + 12));
					index = s.indexOf("dataUIType=\"");
					if (index > 0)
						sType = s.substring(index + 12,
								s.indexOf("\"", index + 13));
					index = s.indexOf("id=\"");
					if (index > 0)
						id = Integer.parseInt(s.substring(index + 4,
								s.indexOf("\"", index + 5)));
					abstractIUnits.add(new AbstractIUnit(id, sName, sDesc,
							sType, parentID));
					parentID = id;
					maxcomponentID = Math.max(maxcomponentID, id);
				}
			}
			// End a group or contain
			else if (s.indexOf("</interactionUnits>") >= 0) // end contain
			{
				for (int i = 0; i < abstractIUnits.size(); i++) {
					if (abstractIUnits.elementAt(i).getId() == parentID) {
						parentID = abstractIUnits.elementAt(i).getParentId();
						break;
					}
				}
			}

		}
		componentId = maxcomponentID;
		componentId++;
		setDimension();
		drawAUIModel();
	}

	/**
	 * Transform a W3C AUI meta-model XML representation of a AUI model into
	 * {@link AbstractIUnit} object in the {@link AEditor#abstractIUnits} list
	 * 
	 * @param sTextContent
	 */
	public void w3cXMLToAIUSObject(String sTextContent) {
		String sType, sName;
		String[] sTempText;
		int index, id = 0, currParentID = 0;
		int maxcomponentID = -1;
		// HashMap link an AUI with its parent, AUI id as key, parent id as
		// value
		HashMap<Integer, Integer> childrenParent = new HashMap<Integer, Integer>();

		sTextContent = sTextContent.replaceAll("&lt;", "<");
		sTextContent = sTextContent.replaceAll("&gt;", ">");
		abstractIUnits.removeAllElements();
		sTextContent = sTextContent.substring(sTextContent.indexOf("\n") + 1,
				sTextContent.length());
		sTextContent = sTextContent.substring(sTextContent.indexOf("\n") + 1,
				sTextContent.length());
		sTextContent = sTextContent.substring(sTextContent.indexOf("\n") + 1,
				sTextContent.length());
		sTextContent = sTextContent.substring(sTextContent.indexOf("\n") + 1,
				sTextContent.length());
		sTempText = sTextContent.split("\n");

		// create all the abstractIUnits
		for (String line : sTempText) {
			if (line.contains("<ns1:abstractInteractionUnit")) {
				index = line.indexOf("id=\"");
				id = Integer.parseInt(line.substring(index + 4,
						line.indexOf("\"", index + 5)));
				currParentID = id;
				index = line.indexOf("name=\"");
				sName = line
						.substring(index + 6, line.indexOf("\"", index + 7));
				index = line.indexOf("role=\"");
				sType = line
						.substring(index + 6, line.indexOf("\"", index + 7));
				abstractIUnits.add(new AbstractIUnit(id, sName, "", sType, 0));
				maxcomponentID = Math.max(maxcomponentID, id);
			} else if (line.contains("<ns1:associatedWith>")) {
				index = line.indexOf("<ns1:associatedWith>");
				childrenParent.put(Integer.parseInt(line.substring(index + 20,
						line.indexOf("</ns1:associatedWith>", index + 21))),
						currParentID);
			}
		}

		// set parent id to all task once their are all loaded from the xml file
		for (AbstractIUnit aiu : abstractIUnits) {
			if (childrenParent.containsKey(aiu.getId()))
				aiu.setParentID(childrenParent.get(aiu.getId()));
		}

		componentId = maxcomponentID;
		componentId++;
		abstractIUnits.elementAt(0).setParentID(-1);
		setDimension();
		drawAUIModel();
	}

	/**
	 * If the task in argument is a leaf task
	 * 
	 * @param task
	 * @return
	 */
	private boolean isLeafTask(UsiXMLTask task) {
		for (int i = 0; i < getTasks().size(); i++) {
			if (task.getTaskID() == getTasks().elementAt(i).getParentID())
				return false;
		}
		return true;
	}

	/**
	 * set the graphical options of the editor
	 * 
	 * @param iNumber
	 *            number of AIU per line
	 * @param w
	 *            width of the leaf AIU
	 * @param h
	 *            height of the leaf AIU
	 * @param widthBetweenAIU
	 *            width between AIU
	 * @param heightBetweenAIU
	 *            height between AIU
	 */
	public void setGraphicalOption(int iNumber, int w, int h,
			int widthBetweenAIU, int heightBetweenAIU) {
		columnNumber = iNumber;
		Width = w;
		Height = h;
		this.widthBetweenAIU = widthBetweenAIU;
		this.heightBetweenAIU = heightBetweenAIU;
		setDimension();
		drawAUIModel();
	}

	/**
	 * load a new AUI model into the editor
	 * 
	 * @param ibutton
	 *            if 1 save the current AUI model before creating a new one, -1
	 *            delete the current AUI model
	 */
	public void newAUIModel(int ibutton) {
		if (ibutton == -1 || ibutton == 1) {
			// save dialog to save the current AUI model
			if (ibutton == 1)
				new MySaveDialog(this).show();
			// reset the editor AUI model
			abstractIUnits.removeAllElements();
			componentId = 0;
			AbstractIUnit aUIObject = new AbstractIUnit(componentId++,
					"MainContain", "MaintContain", "Contain", -1);
			abstractIUnits.add(aUIObject);
			new AIUEdition(this, aUIObject, false).show();
			setDimension();
			drawAUIModel();
		}
	}

	/**
	 * return the task model loaded in the editor
	 * 
	 * @return {@link AEditor#tasks}
	 */
	public Vector<UsiXMLTask> getTasks() {
		return this.tasks;
	}
}
