package auiEditor.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.dom.client.Document;
import com.google.gwt.user.client.ui.Frame;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class AUIEditor implements EntryPoint {
	private static final String DOWNLOAD_IFRAME = "__gwt_downloadFrame";
	private static Frame downloadFrame;

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		downloadFrame = Frame.wrap(Document.get().getElementById(
				DOWNLOAD_IFRAME));
		AEditor myEditor = new AEditor();
		myEditor.setPopupPosition(100, 10);
		myEditor.show();
	}
	/**
	 * Load a download URL into a hidden IFrame
	 * @param url
	 * 			the URL at which the file should be downloaded
	 */
	public static void downloadURL(String url) {
		downloadFrame.setUrl(url);
	}
}
