package auiEditor.client;

import java.util.HashMap;

/**
 * TaskAdaptationConstant is the class storing all the static value necessary to AUI model generation from task model
 * It contains weight for platform adaptation and mental workload for mental workload adaptation
 * @author Jonathan Ricetti
 *
 */
public class TaskAdaptationConstant {
	
		/**
		* Weight for a small screen device
		*/
		public static final int lowWeight = 5;
		
		/**
		 * Weight for a medium screen device
		 */
		public static final int mediumWeight = 10;
		
		/**
		 * Weight for a big screen device
		 */
		public static final int highWeight = 25;
		
		/**
		 * Mental workload for a beginner
		 */
		public static final int beginner = 6;
		
		/**
		 * Mental workload for a normal user
		 */
		public static final int normal = 10;
		
		/**
		 * Mental workload for a advanced user
		 */
		public static final int advanced = 20;
		
		/**
		 * Mental workload for a professional user
		 */
		public static final int professional = 30;
		
		/**
		 * HashMap with keyword as key and mental workload as value
		 * 
		 * @see TaskAdaptationConstant#TaskAdaptationConstant()
		 */
		public final HashMap<String[], Integer> actionTaskTypes = new HashMap<String[], Integer>();
		
		/**
		 * List of keyword for convey task type
		 */
		private final String[] convey = {"convey", "communicate", "transit", "call", "acknowledge", "respond", "answer", "suggest", "direct", "instruct", "request"} ;
		
		/**
		 * List of keyword for create task type
		 */
		private final String[] create = {"create", "input","encode", "enter", "associate", "name", "introduce", "insert", "assemble", "aggregate", "add"};
		
		/**
		 * List of keyword for delete task type
		 */
		private final String[] delete = {"delete", "eliminate", "remove", "cut", "ungroup", "dissociate"};
		
		/**
		 * List of keyword for duplicate task type
		 */
		private final String[] duplicate = {"duplicate", "copy"};
		
		/**
		 * List of keyword for filter task type
		 */
		private final String[] filter = {"filter", "segregate", "setaside"};
		
		/**
		 * List of keyword for mediate task type
		 */
		private final String[] mediate = {"mediate", "analyze", "synthesize", "compare", "evaluate", "decide"};
		
		/**
		 * List of keyword for modify task type
		 */
		private final String[] modify = {"modify", "change", "alter", "transform", "tuning", "rename", "segregate", "resize", "collapse", "expand"};
		
		/**
		 * List of keyword for move task type
		 */
		private final String[] move = {"move", "relocate", "hide", "show", "position", "orient", "path", "travel"};
		
		/**
		 * List of keyword for navigation task type
		 */
		private final String[] navigation = {"navigation", "goto"};
		
		/**
		 * List of keyword for perceive task type
		 */
		private final String[] perceive = {"perceive", "acquire", "detect", "search", "scan", "extract", "identify", "discriminate", "recognize", "locate", "examine", "monitor", "scan", "detect"};
		
		/**
		 * List of keyword for reinitialize task type
		 */
		private final String[] reinitialize = {"reinitialize", "wipeout", "clear", "erase"};
		
		/**
		 * List of keyword for select task type
		 */
		private final String[] select = {"select", "pick", "choose"};
		
		/**
		 * List of keyword for trigger task type
		 */
		private final String[] trigger = {"trigger", "initiate/start", "play", "active", "execute", "function", "record", "purchase"};
		
		/**
		 * List of keyword for stop task type
		 */
		private final String[] stop = {"stop", "end", "finish", "exit", "suspend", "complete", "terminate", "cancel"};
		
		/**
		 * List of keyword for toggle task type
		 */
		private final String[] toggle = {"toggle", "activate", "deactivate", "Switch"};
		
		/**
		 * Contructor of the {@link TaskAdaptationConstant} </br>
		 * Add the mental workload of the keywords to the  mental workload HashMap
		 * 
		 * @see TaskAdaptationConstant#actionTaskTypes
		 */
		public TaskAdaptationConstant() {
			actionTaskTypes.put(convey, 2);
			actionTaskTypes.put(create, 3);
			actionTaskTypes.put(delete, 4);
			actionTaskTypes.put(duplicate, 2);
			actionTaskTypes.put(filter, 3);
			actionTaskTypes.put(mediate, 0);
			actionTaskTypes.put(modify, 4);
			actionTaskTypes.put(move, 3);
			actionTaskTypes.put(navigation, 3);
			actionTaskTypes.put(perceive, 4);
			actionTaskTypes.put(reinitialize, 0);
			actionTaskTypes.put(select, 5);
			actionTaskTypes.put(trigger, 1);
			actionTaskTypes.put(stop, 1);
			actionTaskTypes.put(toggle, 2);			
		}

		
}
