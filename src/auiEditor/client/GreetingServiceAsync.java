package auiEditor.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GreetingService</code>.
 */
public interface GreetingServiceAsync {
	void greetServer(String input, AsyncCallback<String> callback)
			throws IllegalArgumentException;
	void sendContextFile(String input, AsyncCallback<String> callback)
			throws IllegalArgumentException;
	void verifyUser(String sUserName, String sPassword, AsyncCallback<Boolean> callback)
			throws IllegalArgumentException;
	void isFilenameValid(String file,AsyncCallback<Boolean> callback);
}
