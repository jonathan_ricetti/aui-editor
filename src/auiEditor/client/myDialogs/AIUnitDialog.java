package auiEditor.client.myDialogs;

import auiEditor.client.AEditor;
import auiEditor.client.myObjects.AbstractIUnit;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * {@link DialogBox} used to set the AIU information when creating a new one
 * 
 * @author Vi Tran
 * @author Jonathan Ricetti
 *
 */
@SuppressWarnings("deprecation")
public class AIUnitDialog extends DialogBox {
	/**
	 * cancel button
	 */
	private Button bCancel = new Button("Cancel");;

	/**
	 * Ok button
	 */
	private Button bOk = new Button("Ok");;

	/**
	 * {@link AEditor} where the {@link AIUnitDialog} {@link DialogBox} is called
	 */
	private AEditor f;

	/**
	 * TextBox where the name of the {@link AbstractIUnit} is set
	 * 
	 * @default empty
	 */
	private TextBox tbAIUName = new TextBox();

	/**
	 * TextBox where the description of the {@link AbstractIUnit} is set
	 */
	private TextBox tbAIUDescription = new TextBox();

	private VerticalPanel panel = new VerticalPanel();
	private HorizontalPanel panel1 = new HorizontalPanel();
	private VerticalPanel mainPanel = new VerticalPanel();
	/**
	 * id of the {@link AbstractIUnit}
	 */
	private int id;

	/**
	 * parent id of the {@link AbstractIUnit}
	 */
	private int parentId;

	/**
	 * type of the {@link AbstractIUnit}
	 */
	private String type;

	/**
	 * Constructor of the {@link AIUEdition} {@link DialogBox}
	 * 
	 * @param fd
	 * @param index
	 * @param type
	 * @param ParentID
	 */
	public AIUnitDialog(AEditor fd, int index, String type, int ParentID) {
		// set the DialogBox fields
		this.f = fd;
		this.id = index;
		this.parentId = ParentID;
		this.type = type;
		
		// set DialogBox title and position
		setText("AbstractIUnit information");
		this.setPopupPosition(400, 200);
		//Set DialogBox widgets
		tbAIUName.setPixelSize(150, 15);
		tbAIUDescription.setPixelSize(150, 15);
		Grid grid = new Grid(2, 2);
		grid.setWidget(0, 0, new Label("AbstractIUnit name "));
		grid.setWidget(1, 0, new Label("AbstractIUnit Description "));
		grid.setWidget(0, 1, tbAIUName);
		grid.setWidget(1, 1, tbAIUDescription);
		panel.add(grid);

		bCancel.setPixelSize(70, 25);
		bOk.setPixelSize(70, 25);
		panel1.add(bOk);
		panel1.add(bCancel);

		mainPanel.add(panel);
		mainPanel.add(panel1);

		panel.setPixelSize(250, 100);
		panel1.setPixelSize(250, 40);
		mainPanel.setPixelSize(250, 140);
		panel1.setCellHorizontalAlignment(bCancel, HorizontalPanel.ALIGN_CENTER);
		panel1.setCellHorizontalAlignment(bOk, HorizontalPanel.ALIGN_CENTER);
		
		// add ClickListener to the ok and cancel button
		bCancel.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				hide();
			}
		});
		bOk.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				clickOk();
			}
		});
		setWidget(mainPanel);

		//set the name field selected
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
	        public void execute () {
	        	tbAIUName.setFocus(true);
	        }
		});
	}

	/** 
	 * Add shortcurt supprot to the {@link AIUnitDialog} {@link DialogBox} </br>
	 * ENTER key to validate the creation of the {@link AbstractIUnit} with the current information in the {@link TextBox}s
	 * </br> ESCAPE key to cancel the {@link AbstractIUnit} creation
	 */
	protected void onPreviewNativeEvent(NativePreviewEvent event) {
		super.onPreviewNativeEvent(event);
		switch (event.getTypeInt()) {
		case Event.ONKEYDOWN:
			if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {
				clickOk();
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ESCAPE) {
				AIUnitDialog.this.hide();
			}
			break;
		}
	}
	
	/**
	 * Create an {@link AbstractIUnit} and add it to the current AUI model in {@link AIUnitDialog#f} with the method {@link AEditor#addNewAIUToAUI(String, String, int, String, int)}
	 */
	private void clickOk() {
		if (tbAIUName.getText().length() == 0) {
			new MyMessageDialog("Warning", "Enter AbstractIUnit name, please!")
					.show();
			return;
		}

		f.addNewAIUToAUI(tbAIUName.getText(), tbAIUDescription.getText(),
				AIUnitDialog.this.id, AIUnitDialog.this.type,
				AIUnitDialog.this.parentId);
		hide();
	}
}
