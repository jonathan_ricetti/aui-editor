package auiEditor.client.myDialogs;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Class use to show message to the user in a {@link DialogBox}
 * @author Vi Tran
 * @author Jonathan Ricetti
 *
 */
public class MyMessageDialog extends DialogBox 
{
	
	/**
	 * ok button
	 */
	private Button bOk = new Button("Ok");
	
	/**
	 * contain the message that will be showed to the user
	 */
	private Label lText = new Label("");
	private VerticalPanel p = new VerticalPanel();
	private HorizontalPanel p1 = new HorizontalPanel();
	private HorizontalPanel p2 = new HorizontalPanel();
	
	/**
	 * Constructor of the {@link MyMessageDialog} {@link DialogBox}
	 * @param sTitle title of the message
	 * @param sMessage message that will be showed
	 */
	public MyMessageDialog(String sTitle, String sMessage) {
      // Set the dialog box's caption.
	  this.setPopupPosition(450, 250);
      setText(sTitle);
      lText.setText(sMessage);
      
      
	  bOk.setPixelSize(65, 28);
	  p1.setPixelSize(350, 60);
	  p2.setPixelSize(350, 40);
	  p.setPixelSize(350, 100);
	  p.add(p1);
      p.add(p2);
      p1.add(lText);
      p2.add(bOk);
      
      p1.setCellHorizontalAlignment(lText, HorizontalPanel.ALIGN_LEFT);
      p2.setCellHorizontalAlignment(bOk, HorizontalPanel.ALIGN_CENTER);
      
      bOk.addClickHandler(new ClickHandler() {
		
		@Override
		public void onClick(ClickEvent event) {
			// TODO Auto-generated method stub
			 MyMessageDialog.this.hide();
		}
      });
      setWidget(p);
    }
	/**
	 * Add shortcut support to {@link MyMessageDialog} {@link DialogBox}
	 * </br>ENTER & ESCAPE key : close the message
	 */
	protected void onPreviewNativeEvent(NativePreviewEvent event) {
        super.onPreviewNativeEvent(event);
        switch (event.getTypeInt()) {
            case Event.ONKEYDOWN:
                if(event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER){
                	MyMessageDialog.this.hide();
        		} else if(event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ESCAPE) {
        			MyMessageDialog.this.hide();
        		}
                break;
        }
    }
 }

	  