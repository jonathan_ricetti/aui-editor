package auiEditor.client.myDialogs;

import auiEditor.client.AEditor;
import auiEditor.client.myObjects.AbstractIUnit;

import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * {@link DialogBox} used to edit abstract interaction unit
 * 
 * @author Jonathan Ricetti
 */
@SuppressWarnings("deprecation")
public class AIUEdition extends DialogBox {
	/**
	 * cancel button
	 */
	private Button bCancel = new Button("Cancel");;

	/**
	 * ok button
	 */
	private Button bOk = new Button("Ok");;

	/**
	 * {@link AEditor} where the {@link AIUEdition} {@link DialogBox} is called
	 */
	private AEditor f;

	/**
	 * {@link AbstractIUnit} being edited
	 */
	private AbstractIUnit auiObject;

	/**
	 * true if {@link AIUEdition#auiObject} is a leaf AIU false if its a
	 * container
	 */
	private boolean isLeaf;

	/**
	 * contain the list of possible types for {@link AIUEdition#auiObject}
	 */
	private ListBox lbAIUType = new ListBox();

	/**
	 * contain the description of {@link AIUEdition#auiObject}
	 */
	private TextBox tbAIUDesc = new TextBox();

	/**
	 * contain the name of {@link AIUEdition#auiObject}
	 */
	private TextBox tbAIUName = new TextBox();

	private VerticalPanel panel = new VerticalPanel();
	private HorizontalPanel panel1 = new HorizontalPanel();
	private VerticalPanel mainPanel = new VerticalPanel();

	/**
	 * Constructor of the {@link AIUEdition} {@link DialogBox}
	 * 
	 * @param fd
	 *            {@link AEditor} where the edition was called
	 * @param aIUnit
	 *            {@link AbstractIUnit} that have to be edited
	 * @param isLeaf
	 *            true if {@link AIUEdition#auiObject} is a leaf AIU false if
	 *            its a container
	 */
	public AIUEdition(AEditor fd, AbstractIUnit aIUnit, Boolean isLeaf) {
		// Set the DialogBox field
		this.f = fd;
		this.auiObject = aIUnit;
		this.isLeaf = isLeaf;
		tbAIUName.setText(aIUnit.getName()); // fill the name TextBox
		tbAIUDesc.setText(aIUnit.getDescription()); // fill the description
													// TextBox

		// set a title to the DialogBox and its position
		setText("Abstract Interaction Unit Information");
		this.setPopupPosition(400, 200);

		// set the widget of the DialogBox
		tbAIUName.setPixelSize(150, 15);
		Grid grid = new Grid(4, 2);
		grid.setWidget(0, 0, new Label("AIUnit Name "));
		grid.setWidget(0, 1, tbAIUName);

		grid.setWidget(1, 0, new Label("AIUnit Description "));
		grid.setWidget(1, 1, tbAIUDesc);

		// set the element in list of the available types for the AIU
		if (isLeaf) {
			lbAIUType.addItem(aIUnit.getType());
			if (!aIUnit.getType().contains("input"))
				lbAIUType.addItem("input");
			if (!aIUnit.getType().contains("output"))
				lbAIUType.addItem("output");
			if (!aIUnit.getType().contains("select"))
				lbAIUType.addItem("select");
			if (!aIUnit.getType().contains("trigger"))
				lbAIUType.addItem("trigger");
			lbAIUType.setItemSelected(0, true);
			lbAIUType.setVisibleItemCount(4);
			grid.setWidget(2, 0, new Label("AIUnit type"));
			grid.setWidget(2, 1, lbAIUType);
		}

		// keep setting the widget of the DialogBox
		panel.add(grid);
		bCancel.setPixelSize(70, 25);
		bOk.setPixelSize(70, 25);
		panel1.add(bOk);
		panel1.add(bCancel);

		mainPanel.add(panel);
		mainPanel.add(panel1);

		panel.setPixelSize(250, 100);
		panel1.setPixelSize(250, 40);
		mainPanel.setPixelSize(250, 140);
		panel1.setCellHorizontalAlignment(bCancel, HorizontalPanel.ALIGN_CENTER);
		panel1.setCellHorizontalAlignment(bOk, HorizontalPanel.ALIGN_CENTER);

		// add ClickListener to the button cancel
		bCancel.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				hide();
			}
		});

		// add ClickListener to the button OK
		bOk.addClickListener(new ClickListener() {
			public void onClick(Widget sender) {
				clickOk();
			}
		});
		
		setWidget(mainPanel);
		
		//set the name field selected
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
	        public void execute () {
	        	tbAIUName.setFocus(true);
	        }
		});
	}

	/**
	 * Add keyboard shortcut to the DialogBox: {@link AIUEdition} </br> ENTER
	 * key to accept the modification and close the DialogBox </br> ESCAPE key
	 * to close the DialogBox without changing the AIU information
	 */
	protected void onPreviewNativeEvent(NativePreviewEvent event) {
		super.onPreviewNativeEvent(event);
		switch (event.getTypeInt()) {
		case Event.ONKEYDOWN:
			if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {
				clickOk();
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ESCAPE) {
				AIUEdition.this.hide();
			}
			break;
		}
	}

	/**
	 * Change the {@link AIUEdition#auiObject} information to the current
	 * information inside: </br> {@link AIUEdition#tbAIUName},
	 * {@link AIUEdition#tbAIUDesc}, {@link AIUEdition#lbAIUType} </br> and then
	 * close the DialogBox
	 */
	private void clickOk() {
		if (tbAIUName.getText().length() == 0) {
			new MyMessageDialog("Warning", "Enter AIO name, please!").show();
			return;
		}
		if (tbAIUDesc.getText().length() == 0) {
			AIUEdition.this.auiObject.setDescription(tbAIUName.getText());
		} else
			AIUEdition.this.auiObject.setDescription(tbAIUDesc.getText());
		AIUEdition.this.auiObject.setName(tbAIUName.getText());
		if (AIUEdition.this.isLeaf) {
			AIUEdition.this.auiObject.setType(lbAIUType.getValue(lbAIUType
					.getSelectedIndex()));
		}
		AIUEdition.this.f.drawAUIModel();
		hide();
	}
}
