package auiEditor.client.myDialogs;

import auiEditor.client.AEditor;




import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.ClickListener;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * {@link DialogBox} that allows the user to choose the graphical options of the editor 
 * </br>
 *
 * @author Vi Tran
 */
@SuppressWarnings("deprecation")
public class MyViewOptionDialog extends DialogBox 
{
	/**
	 * Cancel button
	 */
	private Button bCancel  = new Button("Cancel");
	
	/**
	 * Ok button
	 */
	private Button bOk  = new Button("Ok");
	
	/**
	 * {@link AEditor} where the {@link MyViewOptionDialog} {@link DialogBox} is
	 * called
	 */
	private AEditor f;
	private ListBox lbColumnNumber = new ListBox();
	private TextBox tbWidth = new TextBox();
	private TextBox tbHeight = new TextBox();
	private TextBox tbX = new TextBox();
	private TextBox tbY = new TextBox();
	
	private VerticalPanel panel = new VerticalPanel();
	private HorizontalPanel panel1 = new HorizontalPanel();
	private VerticalPanel mainPanel = new VerticalPanel();
	
	/**
	 * Constructor of {@link MyViewOptionDialog} {@link DialogBox}
	 * @param fd {@link AEditor} where the {@link MyViewOptionDialog} was called
	 * @param iColNumber
	 * @param w
	 * @param h
	 * @param x
	 * @param y
	 */
	public MyViewOptionDialog(AEditor fd, int iColNumber, int w, int h, int x, int y) {
      // Set the dialog box's caption.
		this.f = fd;
		setText("Setting view options");
		this.setPopupPosition(400,200);
		tbWidth.setPixelSize(50, 15);
		tbHeight.setPixelSize(50, 15);
		tbX.setPixelSize(50, 15);
		tbY.setPixelSize(50, 15);
		Grid grid = new Grid(5, 2);
		grid.setWidget(0, 0, new Label("Do you want to display AIOs in "));
		grid.setWidget(1, 0, new Label("Width of a AIO(10 - 100px) "));
		grid.setWidget(2, 0, new Label("Height of a AIO(5-50px) "));
		grid.setWidget(3, 0, new Label("Width between two AIOs(5-50px) "));
		grid.setWidget(4, 0, new Label("Height between two AIOs(3-40px) "));
		grid.setWidget(0, 1, lbColumnNumber);
		grid.setWidget(1, 1, tbWidth);
		grid.setWidget(2, 1, tbHeight);
		grid.setWidget(3, 1, tbX);
		grid.setWidget(4, 1, tbY);
		
		tbWidth.setTextAlignment(TextBox.ALIGN_RIGHT);
		tbHeight.setTextAlignment(TextBox.ALIGN_RIGHT);
		tbX.setTextAlignment(TextBox.ALIGN_RIGHT);
		tbY.setTextAlignment(TextBox.ALIGN_RIGHT);
		
		tbWidth.setText(String.valueOf(w));
		tbHeight.setText(String.valueOf(h));
		tbX.setText(String.valueOf(x));
		tbY.setText(String.valueOf(y));
		
		lbColumnNumber.addItem("Unlimited");
		lbColumnNumber.addItem("1");
		lbColumnNumber.addItem("2");
		lbColumnNumber.addItem("3");
		lbColumnNumber.addItem("4");
		lbColumnNumber.addItem("5");
		lbColumnNumber.addItem("6");
		lbColumnNumber.addItem("7");
		lbColumnNumber.addItem("8");
		lbColumnNumber.addItem("9");
		lbColumnNumber.addItem("10");
		
		if (iColNumber == -1) lbColumnNumber.setItemSelected(0, true);
		else lbColumnNumber.setItemSelected(iColNumber, true);
		
		panel.add(grid);
	    
	   
	    
		bCancel.setPixelSize(70, 25);
		bOk.setPixelSize(70, 25);
		panel1.add(bOk);
		panel1.add(bCancel);
		
        mainPanel.add(panel);
        HorizontalPanel p = new HorizontalPanel();
        
        mainPanel.add(p);
        mainPanel.add(panel1);
        
        panel.setPixelSize(300, 150);
        panel1.setPixelSize(300, 40);
        p.setPixelSize(300, 20);
        mainPanel.setPixelSize(300, 210);
        panel1.setCellHorizontalAlignment(bCancel, HorizontalPanel.ALIGN_CENTER);
        panel1.setCellHorizontalAlignment(bOk, HorizontalPanel.ALIGN_CENTER);
        
        // add click listener to the buttons
        bCancel.addClickListener(new ClickListener()  {
            public void onClick(Widget sender) {
                hide();
            }
       });
        
        bOk.addClickListener(new ClickListener()  {
            public void onClick(Widget sender) {
            	clickOk();
            }
       });
       setWidget(mainPanel);
	}
	
	/**
	 * Add shortcut support to {@link MyViewOptionDialog} {@link DialogBox}
	 * </br>
	 * ENTER key : validate the option and close the {@link DialogBox}
	 * ESCAPE key : close the dialog box without saving the options
	 * 
	 * @see MyViewOptionDialog#clickOk()
	 */
	protected void onPreviewNativeEvent(NativePreviewEvent event) {
        super.onPreviewNativeEvent(event);
        switch (event.getTypeInt()) {
            case Event.ONKEYDOWN:
                if(event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER){
        			clickOk();
        		} else if(event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ESCAPE) {
        			MyViewOptionDialog.this.hide();
        		}
                break;
        }
    }
 

	/**
	 * Apply the new graphical option to the {@link AEditor} {@link MyViewOptionDialog#f}
	 */
	private void clickOk(){
		int n = 0, x = 20, y = 20, w = 80, h = 17; // default values
    	try
    	{
    		w = Integer.valueOf(tbWidth.getText()).intValue();
    		h = Integer.valueOf(tbHeight.getText()).intValue();
    		x = Integer.valueOf(tbX.getText()).intValue();
    		y = Integer.valueOf(tbY.getText()).intValue();
    		if (w > 100) w = 100;
    		if (w < 10) w = 10;
    		if (h > 50) h = 50;
    		if (h < 5) h = 5;
    		if (x > 50) x = 50;
    		if (x < 5) x = 5;
    		if (y > 40) y = 40;
    		if (y < 3) y = 3;
    		
    	}
    	catch (NumberFormatException e)
    	{
    		new MyMessageDialog("Warning", "Number format is wrong!").show();
    	}
    	
        if (lbColumnNumber.getSelectedIndex() == 0) n = -1;
        else n = lbColumnNumber.getSelectedIndex();
        f.setGraphicalOption(n, w, h, x, y);
        hide();
    }
}
	  