package auiEditor.client.myDialogs;

import auiEditor.client.AEditor;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Class used to allow the user to chosen between multiple paste options in a
 * {@link DialogBox} and paste the copied or cut AIUs into the AUI model of
 * {@link AEditor} that call the {@link DialogBox}
 * 
 * @author Vi Tran
 * @author Jonathan Ricetti
 *
 */
public class MyPasteDialog extends DialogBox {
	/**
	 * Ok button
	 */
	private Button bOk = new Button("Ok");

	/**
	 * Cancel button
	 */
	private Button bCancel = new Button("Cancel");

	/**
	 * title of the {@link MyPasteDialog}
	 */
	private Label lText = new Label();

	// Widgets of the DialogBox: One RadioButton per paste options
	private RadioButton rb0 = new RadioButton("myRadioGroup",
			"inside of selected AIO");
	private RadioButton rb1 = new RadioButton("myRadioGroup",
			"before the selected AIO");
	private RadioButton rb2 = new RadioButton("myRadioGroup",
			"after the selected AIO");
	private VerticalPanel p = new VerticalPanel();
	private VerticalPanel panel = new VerticalPanel();

	private VerticalPanel p1 = new VerticalPanel();
	private HorizontalPanel p2 = new HorizontalPanel();

	/**
	 * {@link AEditor} where the {@link MyPasteDialog} {@link DialogBox} is
	 * called
	 */
	private AEditor f;

	/**
	 * Constructor of the {@link MyPasteDialog} {@link DialogBox}
	 * 
	 * @param sTitle
	 *            title of the {@link MyPasteDialog}
	 * @param isContainType
	 *            true if the selected AIU is an container false if not
	 * @param f
	 *            {@link AEditor} where the {@link MyPasteDialog} was called
	 */
	@SuppressWarnings("deprecation")
	public MyPasteDialog(String sTitle, boolean isContainType, AEditor f) {
		// Set the dialog box's fields
		this.f = f;
		this.setPopupPosition(450, 250);
		setText(sTitle);

		lText.setText("Do you want to paste your AIOs ");
		rb0.setChecked(true);
		if (!isContainType)
			rb0.setEnabled(false);

		panel.add(rb0);
		panel.add(rb1);
		panel.add(rb2);

		bOk.setPixelSize(65, 28);
		bCancel.setPixelSize(65, 28);
		p1.setPixelSize(350, 100);
		p2.setPixelSize(300, 40);
		p.add(p1);
		p.add(p2);
		p1.add(lText);
		p1.add(panel);

		p2.add(bOk);
		p2.add(bCancel);
		p1.setCellHorizontalAlignment(lText, HorizontalPanel.ALIGN_LEFT);
		p1.setCellHorizontalAlignment(panel, HorizontalPanel.ALIGN_LEFT);
		p2.setCellHorizontalAlignment(bOk, HorizontalPanel.ALIGN_CENTER);
		p2.setCellHorizontalAlignment(bCancel, HorizontalPanel.ALIGN_CENTER);

		// add ClickHandler to the ok and cancel button
		bOk.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				clickOk();
			}
		});

		bCancel.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				MyPasteDialog.this.f.paste(0);
				MyPasteDialog.this.hide();
			}
		});
		setWidget(p);
	}

	/**
	 * Add shortcut support to {@link MyPasteDialog} ENTER key : to paste the
	 * copied or cut AIU(s) with the currently selected option ESCAPE key :
	 * close the {@link MyPasteDialog} without pasting
	 * 
	 * @see MyPasteDialog#clickOk()
	 */
	protected void onPreviewNativeEvent(NativePreviewEvent event) {
		super.onPreviewNativeEvent(event);
		switch (event.getTypeInt()) {
		case Event.ONKEYDOWN:
			if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {
				clickOk();
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ESCAPE) {
				MyPasteDialog.this.hide();
			}
			break;
		}
	}

	/**
	 * method use to paste the copied or cut AIU(s) with the currently
	 * selection option into the AUI model of the {@link AEditor}
	 * {@link MyPasteDialog#f}</br> three types of paste : <br>
	 * inside the selected AIU : paste(0) before the selected AIU : paste(1)
	 * after the selected AIU : paste(2)
	 * 
	 * @see AEditor#paste(int)
	 */
	@SuppressWarnings("deprecation")
	private void clickOk() {
		if (rb0.isChecked())
			MyPasteDialog.this.f.paste(1);
		else if (rb1.isChecked())
			MyPasteDialog.this.f.paste(2);
		else if (rb2.isChecked())
			MyPasteDialog.this.f.paste(3);
		MyPasteDialog.this.hide();
	}
}
