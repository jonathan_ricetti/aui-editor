package auiEditor.client.myDialogs;

import auiEditor.client.AEditor;
import auiEditor.client.GreetingService;
import auiEditor.client.GreetingServiceAsync;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.Scheduler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * This {@link DialogBox} allows the user to choose between the saving format available
 * @author Jonathan Ricetti
 */
public class MySaveDialog extends DialogBox {
	/**
	 * Used to check if the filename is valid
	 * @see {@link GreetingServiceAsync#isFilenameValid(String, AsyncCallback)}
	 */
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);
	
	/**
	 * Save button
	 */
	private Button bSave = new Button("Save");
	
	/**
	 * Cancel button
	 */
	private Button bCancel = new Button("Cancel");
	
	/**
	 * title of the {@link DialogBox}
	 */
	private Label lText = new Label("");
	
	// Widget of the DialogBox
	private RadioButton rb0 = new RadioButton("myRadioGroup",
			"UsiXml AUI model");
	private RadioButton rb1 = new RadioButton("myRadioGroup", "W3C AUI model");
	private VerticalPanel p = new VerticalPanel();
	private VerticalPanel p1 = new VerticalPanel();

	private HorizontalPanel p2 = new HorizontalPanel();
	private HorizontalPanel p21 = new HorizontalPanel();
	private HorizontalPanel p22 = new HorizontalPanel();
	private HorizontalPanel prb = new HorizontalPanel();
	
	/**
	 * Used to contain the filename chosen by the user
	 */
	private TextBox textBox1 = new TextBox();
	
	/**
	 * {@link AEditor} where the {@link MySaveDialog} {@link DialogBox} is
	 * called
	 */
	private AEditor f;

	/**
	 * Constructor of the {@link MySaveDialog} {@link DialogBox}
	 * @param f {@link AEditor} where the {@link DialogBox} was called
	 */
	@SuppressWarnings("deprecation")
	public MySaveDialog(AEditor f) {
		// Set the dialog box's fields
		this.f = f;
		this.setPopupPosition(450, 250);
		
		//set the Dialogbox's widget
		setText("Save AUI into a file");
		lText.setText("Filename :");

		rb0.setChecked(true);
		prb.add(rb0);
		prb.add(rb1);
		bSave.setPixelSize(65, 28);
		bCancel.setPixelSize(65, 28);
		p1.setPixelSize(350, 50);
		p21.setPixelSize(350, 40);
		p22.setPixelSize(350, 40);
		p2.setPixelSize(300, 40);
		prb.setPixelSize(350, 40);
		p21.add(lText);
		p21.add(textBox1);
		p22.add(p21);

		
		p.add(p22);
		p.add(prb);
		p.add(p1);
		p.add(p2);

		p2.add(bSave);
		p2.add(bCancel);

		//add ClickHandler to the buttons
		bSave.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				clickSave();

			}
		});
		bCancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				MySaveDialog.this.hide();
			}
		});
		
		setWidget(p);
		
		//set the filename field selected
		Scheduler.get().scheduleDeferred(new Scheduler.ScheduledCommand () {
	        public void execute () {
	        	textBox1.setFocus(true);
	        }
		});
	}

	/**
	 * Add shortcut support to the {@link MySaveDialog} {@link DialogBox}
	 *</br>
	 *ENTER key: save the AUI model , open a download window with the file
	 *</br>
	 *ESCAPE key: close the {@link DialogBox}
	 */
	protected void onPreviewNativeEvent(NativePreviewEvent event) {
        super.onPreviewNativeEvent(event);
        switch (event.getTypeInt()) {
            case Event.ONKEYDOWN:
                if(event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER){
        			clickSave();
        		} else if(event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ESCAPE) {
        			MySaveDialog.this.hide();
        		}
                break;
        }
    }
	
	/**
	 * if the filename is correct save the AUI model into an XML file in the selected format
	 * 
	 * @see {@link AEditor#saveAUIModel(String, int)}
	 */
	private void clickSave(){
		greetingService.isFilenameValid(textBox1.getText(),
				new AsyncCallback<Boolean>() {
					public void onFailure(Throwable caught) {
						// Show the RPC error message to the user
						Window.alert(caught.getMessage());
					}

					@SuppressWarnings("deprecation")
					public void onSuccess(Boolean result) {
						if (!result || !textBox1.getText().endsWith(".xml")) {
							textBox1.addStyleName("highlightWrong");
							textBox1.setFocus(true);
						} else {
							if (rb0.isChecked())
								MySaveDialog.this.f.saveAUIModel(textBox1.getText(),
										0);
							else if (rb1.isChecked())
								MySaveDialog.this.f.saveAUIModel(textBox1.getText(),
										1);
							MySaveDialog.this.hide();
						}
					}
				});
	}

}
