package auiEditor.client.myDialogs;

import auiEditor.client.AEditor;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.IntegerBox;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RadioButton;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * {@link DialogBox} used to generate an AUI model from a previously loaded task
 * model
 * 
 * @author Jonathan Ricetti
 *
 */
public class AUIGeneratorDialog extends DialogBox {
	/**
	 * Ok button
	 */
	private Button bOK = new Button("Ok");
	/**
	 * Cancel button
	 */
	private Button bCancel = new Button("Cancel");

	/*
	 * Widgets of the DialogBox one RadioButton for each adaptation method
	 * 
	 * 2 RadiosGroup one for global method choice and another for sub option
	 */
	private VerticalPanel p = new VerticalPanel();
	private VerticalPanel p1 = new VerticalPanel();
	private VerticalPanel pv = new VerticalPanel();
	private HorizontalPanel texthp = new HorizontalPanel();
	private HorizontalPanel p2 = new HorizontalPanel();
	private HorizontalPanel p21 = new HorizontalPanel();
	private HorizontalPanel p22 = new HorizontalPanel();
	private RadioButton rb0 = new RadioButton("myRadioGroup", "Low");
	private RadioButton rb1 = new RadioButton("myRadioGroup", "Medium");
	private RadioButton rb2 = new RadioButton("myRadioGroup", "High");
	private RadioButton rb3 = new RadioButton("myRadioGroup",
			"Minimal selection");
	private RadioButton rb4 = new RadioButton("myRadioGroup",
			"Maximal selection");
	private RadioButton rb5 = new RadioButton("myRadioGroup",
			"Custom maximum selection");
	private RadioButton rb6 = new RadioButton("myRadioGroup", "Beginner");
	private RadioButton rb7 = new RadioButton("myRadioGroup", "Normal user");
	private RadioButton rb8 = new RadioButton("myRadioGroup", "Advanced user");
	private RadioButton rb9 = new RadioButton("myRadioGroup", "Professional");
	private RadioButton rb10 = new RadioButton("myRadioGroup",
			"By input/output selection");
	private RadioButton rbLeaf = new RadioButton("myRadioGroup2",
			"Leaf-tasks only mode");
	private RadioButton rbRecursive = new RadioButton("myRadioGroup2",
			"Recursive mode");
	private IntegerBox textBox1 = new IntegerBox();
	private VerticalPanel panel = new VerticalPanel();
	private HorizontalPanel leafRecuHpanel = new HorizontalPanel();

	/**
	 * {@link AEditor} where the {@link AUIGeneratorDialog} {@link DialogBox} is
	 * called
	 */
	private AEditor f;

	/**
	 * Constructor of {@link AUIGeneratorDialog} {@link DialogBox}
	 * 
	 * @param title
	 *            title of the {@link DialogBox}
	 * @param f
	 *            {@link AEditor} where the {@link AUIGeneratorDialog} is called
	 */
	@SuppressWarnings("deprecation")
	public AUIGeneratorDialog(String title, AEditor f) {
		// Set fields of the DialogBox
		this.f = f;
		setText(title);

		// Set DialogBox position
		this.setPopupPosition(450, 250);

		// Preselection one RadioButton for each RadioGroup
		rb0.setChecked(true);
		rbLeaf.setChecked(true);

		// Add Widgets to the DialogBox
		panel.add(new Label("Plarform model adaptation method (weight): "));
		panel.add(rb0);
		panel.add(rb1);
		panel.add(rb2);
		panel.add(new HTML("<hr/>"));
		panel.add(new Label("Limited number of AIU adaptation method: "));
		panel.add(rb3);
		panel.add(rb4);
		panel.add(rb5);
		Label lTextbox1 = new Label("Maximum number of AIU: ");
		texthp.add(lTextbox1);
		texthp.add(textBox1);
		panel.add(texthp);
		leafRecuHpanel.add(rbLeaf);
		leafRecuHpanel.add(rbRecursive);
		panel.add(leafRecuHpanel);
		panel.add(new HTML("<hr/>"));
		panel.add(new Label("Mental worload adaptation method: "));
		panel.add(rb6);
		panel.add(rb7);
		panel.add(rb8);
		panel.add(rb9);
		panel.add(new HTML("<hr/>"));
		panel.add(new Label("Split inputs and outputs adaptation method: "));
		panel.add(rb10);
		panel.add(new HTML("<hr/>"));

		// Set Widgets size
		bOK.setPixelSize(65, 28);
		bCancel.setPixelSize(65, 28);
		pv.setPixelSize(350, 50);
		p21.setPixelSize(50, 40);
		p22.setPixelSize(350, 40);
		p2.setPixelSize(300, 40);

		// Add Widgets do the DialogBox
		p22.add(p21);
		p22.add(p2);
		p.add(p1);
		p.add(pv);
		p.add(p22);
		p1.add(panel);

		// Add button to the DialogBox
		p2.add(bOK);
		p2.add(bCancel);

		// Add ClickHanlder to ok and cancel button
		bOK.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				clickOk();
			}
		});
		bCancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				AUIGeneratorDialog.this.hide();
			}
		});
		setWidget(p);
	}

	/**
	 * Add shortcut support to the {@link AUIGeneratorDialog} {@link DialogBox}
	 * </br> ENTER key : generate the AUI model for the selection adaptation
	 * method </br> ESCAPE key : close the {@link DialogBox} without generating
	 * the AUI model
	 * 
	 * @see AUIGeneratorDialog#clickOk()
	 * 
	 */

	protected void onPreviewNativeEvent(NativePreviewEvent event) {
		super.onPreviewNativeEvent(event);
		switch (event.getTypeInt()) {
		case Event.ONKEYDOWN:
			if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ENTER) {
				clickOk();
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ESCAPE) {
				AUIGeneratorDialog.this.hide();
			}
			break;
		}
	}

	/**
	 * Method call to create the AUI model with the selected AUI model the
	 * {@link AEditor} {@link AUIGeneratorDialog#f} </br> Determine the
	 * adaptation method to apply according to the check {@link RadioButton} </br>
	 *  m field = adaptation methods </br> 0 platform model adaptation </br> 1 Limited number of AIU, 2 Limited number of AIU leaf-only </br> 3 mental workload adaptation method </br> 4 split inputs and outputs adaptation method
	 *  n field = sub options for adaptation method: </br> Platform model: 2 for small
	 *      screen, 3 medium, 4 big </br> Limited number of AIU: 1 for minimal
	 *      selection , 2 * number of tasks for maximal selection , custom
	 *      number loaded for custom adaptation </br>
	 *      MentalWorkload adaptation : 1 for beginner, 2 for normal user, 3 for advanced user, 4 for a professional user <br>
	 *      
	 * @see AEditor#tasksToAUIs(int, int)
	 */
	@SuppressWarnings("deprecation")
	private void clickOk() {

		int m = 0;

		/**

		 */
		int n = 0;

		if (rb0.isChecked())
			n = 2;
		else if (rb1.isChecked())
			n = 3;
		else if (rb2.isChecked())
			n = 4;
		else if (rb3.isChecked()) {
			if (rbLeaf.isChecked())
				m = 2;
			else
				m = 1;
			n = 1;
		} else if (rb4.isChecked()) {
			if (rbLeaf.isChecked())
				m = 2;
			else
				m = 1;
			n = 2 * AUIGeneratorDialog.this.f.getTasks().size();
		} else if (rb5.isChecked()) {
			if (rbLeaf.isChecked())
				m = 2;
			else
				m = 1;
			n = Integer.parseInt(textBox1.getText());
		} else if (rb6.isChecked() || rb7.isChecked() || rb8.isChecked()
				|| rb9.isChecked()) {
			m = 3;
			if (rb6.isChecked())
				n = 1;
			else if (rb7.isChecked())
				n = 2;
			else if (rb8.isChecked())
				n = 3;
			else if (rb9.isChecked())
				n = 4;
		} else if (rb10.isChecked())
			m = 4;
		AUIGeneratorDialog.this.f.tasksToAUIs(m, n);
		AUIGeneratorDialog.this.hide();
	}
}
