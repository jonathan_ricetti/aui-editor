package auiEditor.client.myDialogs;

import auiEditor.client.AEditor;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * {@link DialogBox} used to show yes/no dialog to the user </br>
 * Result depend form the purpose with which the {@link MyYesNoDialog} was called </br>
 * Save: save or not the current AUI model
 * Delete: delete or not the element inside the AIU container
 * Copy/Cut : copy/cut the element inside the AIU container or not
 * @author Vi Tran
 * @author Jonathan Ricetti
 *
 */
public class MyYesNoDialog extends DialogBox {
	/**
	 * Yes button
	 */
	private Button bYes = new Button("Yes");
	
	/**
	 * No button
	 */
	private Button bNo = new Button("No");
	
	/**
	 * Cancel button
	 */
	private Button bCancel = new Button("Cancel");
	
	/**
	 * question of the {@link MyYesNoDialog}
	 */
	private Label lText = new Label("");
	
	//Widgets of the DialogBox
	private VerticalPanel p = new VerticalPanel();
	private VerticalPanel p1 = new VerticalPanel();
	private HorizontalPanel p2 = new HorizontalPanel();
	private HorizontalPanel p21 = new HorizontalPanel();
	private HorizontalPanel p22 = new HorizontalPanel();
	
	/**
	 * {@link AEditor} where the {@link MyYesNoDialog} {@link DialogBox} is
	 * called
	 */
	private AEditor f;
	
	/**
	 * Message show to the user with the purpose of the {@link MyYesNoDialog}
	 */
	private String sPurpose;

	/**
	 * Constructor of the {@link MyYesNoDialog} {@link DialogBox}
	 * @param title title of the {@link DialogBox}
	 * @param question question of the {@link MyYesNoDialog}
	 * @param f {@link AEditor} where the {@link DialogBox} was called
	 * @param purpose purpose of the {@link MyYesNoDialog}
	 */
	public MyYesNoDialog(String title, String question, AEditor f,
			String purpose) {
		// Set the dialog box's fields
		this.f = f;
		this.sPurpose = purpose;
		this.setPopupPosition(450, 250);
		setText(title);
		lText.setText(question);
		
		//Set the widgets of the DialogBox
		bYes.setPixelSize(65, 28);
		bNo.setPixelSize(65, 28);
		bCancel.setPixelSize(65, 28);
		p1.setPixelSize(350, 50);
		p21.setPixelSize(50, 40);
		p22.setPixelSize(350, 40);
		p2.setPixelSize(300, 40);
		p22.add(p21);
		p22.add(p2);
		p.add(p1);
		p.add(p22);
		p1.add(lText);

		p2.add(bYes);
		p2.add(bNo);
		p2.add(bCancel);

		// Add ClickHandler  to the buttons
		bYes.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				clickYes();
			}
		});

		bNo.addClickHandler(new ClickHandler() {

			public void onClick(ClickEvent event) {
				clickNo();
			}
		});
		bCancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				MyYesNoDialog.this.hide();
			}
		});
		
		setWidget(p);
	}

	/**
	 * Add shortCut support to the {@link MyYesNoDialog} {@link DialogBox}
	 * </br>
	 * Y key: call {@link MyYesNoDialog#clickYes()} </br>
	 * N key: call {@link MyYesNoDialog#clickNo()}</br>
	 * ESCAPE key: close the {@link DialogBox}
	 */
	protected void onPreviewNativeEvent(NativePreviewEvent event) {
		super.onPreviewNativeEvent(event);
		switch (event.getTypeInt()) {
		case Event.ONKEYDOWN:
			if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_Y) {
				clickYes();
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_N) {
				clickNo();
			} else if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ESCAPE) {
				MyYesNoDialog.this.hide();
			}
			break;
		}
	}

	/**
	 * Method called when pressing Y or clicking the Yes button </br>
	 * 
	 * Result depend from the purpose: delete, save, copy or cut
	 */
	private void clickYes() {
		if (MyYesNoDialog.this.sPurpose.indexOf("delete") >= 0)
			MyYesNoDialog.this.f.deleteSelectedAUI(1);
		if (MyYesNoDialog.this.sPurpose.indexOf("save") >= 0)
			MyYesNoDialog.this.f.newAUIModel(1);
		if (MyYesNoDialog.this.sPurpose.indexOf("copy") >= 0) {
			MyYesNoDialog.this.f.copyAndCutAUIs(true);
			MyYesNoDialog.this.f.setCopyMode();
		}
		if (MyYesNoDialog.this.sPurpose.indexOf("cut") >= 0) {
			MyYesNoDialog.this.f.copyAndCutAUIs(true);
			MyYesNoDialog.this.f.setCutMode();
		}
		MyYesNoDialog.this.hide();
	}

	/**
	 * Method called when pressing N or clicking the No button </br>
	 * 
	 * Result depend from the purpose: delete, save, copy or cut
	 */
	private void clickNo() {
		if (MyYesNoDialog.this.sPurpose.indexOf("delete") >= 0)
			MyYesNoDialog.this.f.deleteSelectedAUI(-1);
		if (MyYesNoDialog.this.sPurpose.indexOf("save") >= 0)
			MyYesNoDialog.this.f.newAUIModel(-1);
		if (MyYesNoDialog.this.sPurpose.indexOf("copy") >= 0) {
			MyYesNoDialog.this.f.copyAndCutAUIs(false);
			MyYesNoDialog.this.f.setCopyMode();
		}
		if (MyYesNoDialog.this.sPurpose.indexOf("cut") >= 0) {
			MyYesNoDialog.this.f.copyAndCutAUIs(false);
			MyYesNoDialog.this.f.setCutMode();
		}
		MyYesNoDialog.this.hide();
	}
}
