package auiEditor.client.myDialogs;

import auiEditor.client.AEditor;

import com.google.gwt.user.client.Event.NativePreviewEvent;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.FileUpload;
import com.google.gwt.user.client.ui.FormHandler;
import com.google.gwt.user.client.ui.FormPanel;
import com.google.gwt.user.client.ui.FormSubmitCompleteEvent;
import com.google.gwt.user.client.ui.FormSubmitEvent;
import com.google.gwt.user.client.ui.Grid;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeEvent;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.Window;

/**
 * Class used to select and open AUI model XML file.
 * 
 * @author Jonathan Ricetti
 *
 */
@SuppressWarnings("deprecation")
public class OpenAuiXml extends DialogBox {
	// widgets of the dialogBox
	private FormPanel fp;
	private VerticalPanel p = new VerticalPanel();

	/**
	 * widgets responsible of file upload
	 */
	private FileUpload upload;

	/**
	 * String representation of the file returned by the upload Servlet
	 */
	private String sResult;

	/**
	 * Cancel button
	 */
	private Button bCancel;

	/**
	 * {@link AEditor} where the {@link OpenAuiXml} {@link DialogBox} is called
	 */
	private AEditor f;

	/**
	 * Constructor of the {@link OpenAuiXml} {@link DialogBox}
	 * 
	 * @param title
	 *            title of the {@link OpenAuiXml}
	 * @param fd
	 *            {@link AEditor} where the {@link DialogBox} is called
	 */
	public OpenAuiXml(String title, AEditor fd) {
		// set the dialog box's field
		this.f = fd;
		setText(title);
		this.setPopupPosition(400, 200);

		// set the dialog box's widgets
		fp = new FormPanel();
		upload = new FileUpload();
		bCancel = new Button("Cancel");
		bCancel.setPixelSize(70, 25);

		fp.setPixelSize(300, 120);
		Grid grid = new Grid(5, 1);
		grid.setWidget(2, 0, upload);
		p.setPixelSize(260, 30);
		p.add(bCancel);
		p.setCellHorizontalAlignment(bCancel, HorizontalPanel.ALIGN_CENTER);
		grid.setWidget(4, 0, p);

		upload.setName("Open File");

		upload.setEnabled(true);
		upload.setTitle("Open file :");

		// add ChangeHandle to the FileUpload
		upload.addChangeHandler(new ChangeHandler() {
			public void onChange(ChangeEvent event) {
				if (upload.getFilename().endsWith(".xml")) {
					fp.submit();
				} else
					Window.alert("Choose a .xml file, please!");

			}
		});

		// add ClickHandler to the cancel button
		bCancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				hide();

			}
		});

		// set the widget parameter for the Post method to the FileUpload
		// servlet
		fp.setAction(GWT.getModuleBaseURL() + "openfile");
		fp.setEncoding(FormPanel.ENCODING_MULTIPART);
		fp.setMethod(FormPanel.METHOD_POST);
		fp.setWidget(grid);
		fp.setStyleName("formPanel");
		fp.addFormHandler(new FormHandler() {
			public void onSubmitComplete(final FormSubmitCompleteEvent event) {
				sResult = event.getResults().toString();
				f.xmlToAIUsObject(sResult);
				hide();
			}

			public void onSubmit(final FormSubmitEvent event) {

			}
		});
		setWidget(fp);
	}

	/**
	 * Add shortcut support to the {@link OpenAuiXml} {@link DialogBox} </br>
	 * ESCAPE key: close the dialog box without loading a file
	 */
	protected void onPreviewNativeEvent(NativePreviewEvent event) {
		super.onPreviewNativeEvent(event);
		switch (event.getTypeInt()) {
		case Event.ONKEYDOWN:
			if (event.getNativeEvent().getKeyCode() == KeyCodes.KEY_ESCAPE) {
				OpenAuiXml.this.hide();
			}
			break;
		}
	}
}
