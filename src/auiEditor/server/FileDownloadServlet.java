package auiEditor.server;

import java.io.IOException;
import java.io.PrintStream;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Java Servlet used to offer file download services to the user
 * @author Jonathan Ricetti
 *
 */
public class FileDownloadServlet extends HttpServlet {
	private static final Logger log = Logger
			.getLogger(FileDownloadServlet.class.getName());
	private static final long serialVersionUID = 1L;
	
	/**
	 * name of the file that will be downloaded by the user
	 * @default default.xml
	 */
	private static String fileName = "default.xml";
	
	/**
	 * String representation of the AUI model that will be download by the user
	 * @default "defaultxml"
	 */
	private static String AUIXML = "defaultxml";

	/**
	 * Post request used to set the file name and the content as a string
	 * @param req should container two parameters: </br> "fileName" with the file name </br> "AUIXML" with the content of the future file as a string
	 * @param resp no response from this request
	 */
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		fileName = req.getParameter("fileName");
		AUIXML = req.getParameter("AUIXML");

	}

	/**
	 * Get request used to force the Servlet to generate the file and to return the download URL to the user
	 * 
	 * @param req no parameter needed
	 * @param resp return the URL where the file can be downloaded
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("application/xml");
		resp.setHeader("Content-Disposition", "attachment; filename="
				+ fileName);
		try {
			log.warning("Got a file to save, name =" + fileName);
			java.io.OutputStream out = resp.getOutputStream();
			final PrintStream printStream = new PrintStream(out);
			printStream.print(AUIXML);
			printStream.flush();
			printStream.close();
			out.flush();
			out.close();
		} catch (Exception e) {
			System.out
					.println(" Data cannot be imported :: getImport() :: ImportExportData "
							+ e);
		}// catch

	}
}
