This document explain how to import the project into eclipse and how to generated the war file used to deploy the editor on an apache tomcat server.

JavaDoc can be found : in the doc folder of the zipfile or here : jonathan_ricetti.bitbucket.org/AUIEditorJavadoc/

To import the project into your eclipse workspace  (you have the gwt pluggin installed in eclipse see : https://developers.google.com/eclipse/docs/download): 
- File -> Import
- Existing project into eclipse
- Import from archive file
- Select the AUIEditor.zip file
- Finish, it is done you have imported the editor into eclipse

To deploy the editor on an apache tomcat server you need a war file. To generate this file you first need to compile the project
- right click on the project name
- google => GWT Compile
Now eclipse compile the project , you can now use the ant buildfile provide to generate the war file
- right click on warbuilder.xml
- Run as -> Ant Build
You war file is now ready to be imported into you apache tomcat server

(use this tutorial to install apache tomcat server on a server running on debian : https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-apache-tomcat-on-a-debian-server )

Now to deploy the editor on you apache server you just have to go to the webapp manager, and import the war file.
If you achieved all these steps congratulation you have a working AUI editor on your server. 